using CommonConstant;
using LogUtil;
using System;
using System.Data.SQLite;
using System.IO;
using Utils;

namespace DAL
{
	public class DBHelper
	{
		public static object DBLocker = new object();

		public static void CreateDatabase()
		{
			try
			{
				if (!Directory.Exists(DBConstant.DB_DIR))
				{
					Directory.CreateDirectory(DBConstant.DB_DIR);
				}
				SQLiteConnection.CreateFile(DBConstant.DB_DIR + "generalattendance.db");
				new SQLiteConnection(DBConstant.DB_DIR + "generalattendance.db");
			}
			catch (Exception ex)
			{
				LogHelper.LogError(typeof(DBHelper), ex);
			}
		}

		public static void CreateTables()
		{
			DBHelper.CreateMainInfo();
			DBHelper.CreateFeatureInfo();
			DBHelper.CreateAttendanceRecord();
			DBHelper.CreateAttendanceRuleRecord();
		}

		public static int CreateIndexBySQL(string sql)
		{
			try
			{
				using (SQLiteConnection conn = new SQLiteConnection(new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString()))
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					SQLiteCommand expr_3C = conn.CreateCommand();
					expr_3C.CommandText = sql;
					int res = expr_3C.ExecuteNonQuery();
					expr_3C.Dispose();
					conn.Close();
					return res;
				}
			}
			catch (Exception ex)
			{
				LogHelper.LogError(typeof(DBHelper), ex);
			}
			return 0;
		}

		public static int CreateMainInfo()
		{
			try
			{
				using (SQLiteConnection conn = new SQLiteConnection(new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString()))
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = "CREATE TABLE IF NOT EXISTS MainInfo (id INTEGER PRIMARY KEY,person_name TEXT NOT NULL,show_image_name TEXT,p_left INTEGER,p_top INTEGER,p_right INTEGER,p_bottom INTEGER,p_orient INTEGER,person_serial TEXT NOT NULL,p_valid INTEGER,add_time TEXT,update_time TEXT,dept TEXT);";
					SQLiteCommand expr_42 = conn.CreateCommand();
					expr_42.CommandText = sql;
					int res = expr_42.ExecuteNonQuery();
					expr_42.Dispose();
					conn.Close();
					return res;
				}
			}
			catch (Exception e)
			{
				LogHelper.LogError(typeof(DBHelper), e);
				Console.WriteLine(e);
			}
			return 0;
		}

		public static int CreateFeatureInfo()
		{
			try
			{
				using (SQLiteConnection conn = new SQLiteConnection(new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString()))
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = "CREATE TABLE IF NOT EXISTS FeatureInfo (id INTEGER PRIMARY KEY,person_id INTEGER NOT NULL,face_id INTEGER,regis_image_name TEXT,f_left INTEGER,f_top INTEGER,f_right INTEGER,f_bottom INTEGER,f_orient INTEGER,f_valid INTEGER,feature blob,feature_size INTEGER);";
					SQLiteCommand expr_42 = conn.CreateCommand();
					expr_42.CommandText = sql;
					int res = expr_42.ExecuteNonQuery();
					expr_42.Dispose();
					conn.Close();
					return res;
				}
			}
			catch (Exception e)
			{
				LogHelper.LogError(typeof(DBHelper), e);
				Console.WriteLine(e);
			}
			return 0;
		}

		public static int CreateAttendanceRecord()
		{
			try
			{
				using (SQLiteConnection conn = new SQLiteConnection(new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "CheckRecord.db"
				}.ToString()))
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = "CREATE TABLE IF NOT EXISTS AttendRecord (id INTEGER PRIMARY KEY,person_id INTEGER,person_serial TEXT,date TEXT,start_time TEXT,end_time TEXT,start_attend_url TEXT,end_attend_url TEXT,attend_status TEXT,add_time TEXT);";
					SQLiteCommand expr_42 = conn.CreateCommand();
					expr_42.CommandText = sql;
					int res = expr_42.ExecuteNonQuery();
					expr_42.Dispose();
					conn.Close();
					return res;
				}
			}
			catch (Exception e)
			{
				LogHelper.LogError(typeof(DBHelper), e);
				Console.WriteLine(e);
			}
			return 0;
		}

		public static int CreateAttendanceRuleRecord()
		{
			try
			{
				using (SQLiteConnection conn = new SQLiteConnection(new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString()))
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = "CREATE TABLE IF NOT EXISTS AttendRule (id INTEGER PRIMARY KEY,json TEXT,start_date TEXT,end_date TEXT);";
					SQLiteCommand command = conn.CreateCommand();
					command.CommandText = sql;
					int res = command.ExecuteNonQuery();
					if (res >= 0)
					{
						sql = "insert into AttendRule values(null,'{\"work_hours_limit\":\"8\",\"attend_rule_mode\":\"0\",\"attend_start_time\":\"09:00\",\"attend_end_time\":\"18:00\",\"effective_time_start\":\"00:00:00\",\"effective_time_end\":\"23:59:59\",\"rest_day\":\"6-7\",\"effective_mode\":0}','" + DateTime.Now.ToString("yyyy-MM-dd") + "','')";
						command.CommandText = sql;
						res = command.ExecuteNonQuery();
					}
					command.Dispose();
					conn.Close();
					return res;
				}
			}
			catch (Exception e)
			{
				LogHelper.LogError(typeof(DBHelper), e);
				Console.WriteLine(e);
			}
			return 0;
		}

		public static void closeConn(SQLiteConnection conn)
		{
			if (conn != null)
			{
				try
				{
					conn.Close();
				}
				catch (Exception e)
				{
					LogHelper.LogError(typeof(DBHelper), e);
					Console.WriteLine(e);
				}
			}
		}
	}
}
