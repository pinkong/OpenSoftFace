using CommonConstant;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	public class MyButton : Button
	{
		private Color _enterColor = ColorUtil.Blue;

		private Color _enterForeColor = ColorUtil.White;

		private Color _leaveBackColor = ColorUtil.Blue;

		private Color _leaveForeColor = ColorUtil.Blue;

		[Category("Appearance"), DefaultValue(typeof(Color), "1, 119, 213"), Description("组件的鼠标进入时的背景颜色")]
		public Color EnterBackColor
		{
			get
			{
				return this._enterColor;
			}
			set
			{
				this._enterColor = value;
				base.Invalidate();
			}
		}

		[Category("Appearance"), Description("组件的鼠标进入时的字体颜色")]
		public Color EnterForeColor
		{
			get
			{
				return this._enterForeColor;
			}
			set
			{
				this._enterForeColor = value;
				base.Invalidate();
			}
		}

		[Category("Appearance2"), Description("组件的鼠标离开时的背景颜色")]
		public Color LeaveBackColor
		{
			get
			{
				return this._leaveBackColor;
			}
			set
			{
				this._leaveBackColor = value;
				base.Invalidate();
			}
		}

		[Category("Appearance2"), Description("组件的鼠标离开时的字体颜色")]
		public Color LeaveForeColor
		{
			get
			{
				return this._leaveForeColor;
			}
			set
			{
				this._leaveForeColor = value;
				base.Invalidate();
			}
		}

		public MyButton()
		{
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);
			base.FlatAppearance.MouseOverBackColor = this._enterColor;
			this.ForeColor = this._enterForeColor;
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave(e);
			base.FlatAppearance.MouseOverBackColor = this._leaveBackColor;
			this.ForeColor = this._leaveForeColor;
		}

		protected override void OnClick(EventArgs e)
		{
			base.OnClick(e);
			bool flag = base.Parent != null;
			if (flag)
			{
				base.Parent.Focus();
			}
		}
	}
}
