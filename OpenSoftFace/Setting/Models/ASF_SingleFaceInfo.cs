using System;

namespace OpenSoftFace.Setting.Models
{
	public struct ASF_SingleFaceInfo
	{
		public MRECT faceRect;

		public int faceOrient;
	}
}
