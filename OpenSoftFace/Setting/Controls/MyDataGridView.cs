using CommonConstant;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	public class MyDataGridView : DataGridView
	{
		private bool _cellColorOnchange = false;

		private Color _cell_color = ColorUtil.Yellow;

		private Color _color_grid = ColorUtil.Beige;

		private Color color_default;

		public bool CellColorOnchange
		{
			get
			{
				return this._cellColorOnchange;
			}
			set
			{
				this._cellColorOnchange = value;
			}
		}

		public Color DefaultcolorSet
		{
			get
			{
				return this._cell_color;
			}
			set
			{
				this._cell_color = value;
			}
		}

		public Color ContentGridcolor
		{
			get
			{
				return this._color_grid;
			}
			set
			{
				this._color_grid = value;
			}
		}

		public MyDataGridView()
		{
			base.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
		}

		protected override void OnCreateControl()
		{
			base.EnableHeadersVisualStyles = false;
			base.ColumnHeadersDefaultCellStyle.BackColor = this._color_grid;
			base.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
			base.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			base.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			base.ColumnHeadersDefaultCellStyle.ForeColor = SystemColors.WindowText;
			base.ColumnHeadersDefaultCellStyle.SelectionBackColor = SystemColors.Highlight;
			base.ColumnHeadersDefaultCellStyle.SelectionForeColor = SystemColors.HighlightText;
			base.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			base.RowHeadersDefaultCellStyle.BackColor = this._color_grid;
			base.RowHeadersDefaultCellStyle.ForeColor = SystemColors.WindowText;
			base.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
			base.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			base.DefaultCellStyle.SelectionBackColor = SystemColors.Highlight;
			base.DefaultCellStyle.SelectionForeColor = SystemColors.HighlightText;
			base.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
			base.BackgroundColor = SystemColors.Window;
			base.BorderStyle = BorderStyle.Fixed3D;
			base.AllowUserToOrderColumns = true;
			base.AutoGenerateColumns = true;
			base.OnCreateControl();
		}

		protected override void OnCellMouseMove(DataGridViewCellMouseEventArgs e)
		{
			base.OnCellMouseMove(e);
			try
			{
				bool cellColorOnchange = this._cellColorOnchange;
				if (cellColorOnchange)
				{
					base.Rows[e.RowIndex].DefaultCellStyle.BackColor = this._cell_color;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		protected override void OnCellMouseEnter(DataGridViewCellEventArgs e)
		{
			base.OnCellMouseEnter(e);
			try
			{
				bool cellColorOnchange = this._cellColorOnchange;
				if (cellColorOnchange)
				{
					this.color_default = base.Rows[e.RowIndex].DefaultCellStyle.BackColor;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		protected override void OnCellMouseLeave(DataGridViewCellEventArgs e)
		{
			base.OnCellMouseLeave(e);
			try
			{
				bool cellColorOnchange = this._cellColorOnchange;
				if (cellColorOnchange)
				{
					base.Rows[e.RowIndex].DefaultCellStyle.BackColor = this.color_default;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void InitializeComponent()
		{
			((ISupportInitialize)this).BeginInit();
			base.SuspendLayout();
			base.RowTemplate.Height = 23;
			((ISupportInitialize)this).EndInit();
			base.ResumeLayout(false);
		}

		protected override void OnCellPainting(DataGridViewCellPaintingEventArgs e)
		{
			base.OnCellPainting(e);
			try
			{
				using (SolidBrush solidBrush = new SolidBrush(this._color_grid))
				{
					using (Pen pen = new Pen(this._color_grid, 1f))
					{
						bool flag = e.ColumnIndex == -1 && e.RowIndex == -1;
						if (flag)
						{
							using (new LinearGradientBrush(e.CellBounds, ColorUtil.Gray, ColorUtil.Gray, LinearGradientMode.ForwardDiagonal))
							{
								e.Graphics.FillRectangle(solidBrush, e.CellBounds);
								Rectangle cellBounds = e.CellBounds;
								cellBounds.Offset(new Point(-1, -1));
								e.Graphics.DrawRectangle(Pens.Gray, cellBounds);
							}
							e.PaintContent(e.CellBounds);
							e.Handled = true;
						}
						else
						{
							bool flag2 = e.RowIndex == -1;
							if (flag2)
							{
								using (new LinearGradientBrush(e.CellBounds, ColorUtil.Silver, ColorUtil.Silver, LinearGradientMode.Vertical))
								{
									e.Graphics.FillRectangle(solidBrush, e.CellBounds);
									Rectangle cellBounds2 = e.CellBounds;
									cellBounds2.Offset(new Point(-1, -1));
									e.Graphics.DrawRectangle(Pens.Silver, cellBounds2);
								}
								e.PaintContent(e.CellBounds);
								e.Handled = true;
							}
							else
							{
								bool flag3 = e.ColumnIndex == -1;
								if (flag3)
								{
									using (new LinearGradientBrush(e.CellBounds, ColorUtil.Silver, ColorUtil.Silver, LinearGradientMode.Horizontal))
									{
										e.Graphics.FillRectangle(solidBrush, e.CellBounds);
										Rectangle cellBounds3 = e.CellBounds;
										cellBounds3.Offset(new Point(-1, -1));
										e.Graphics.DrawRectangle(Pens.Silver, cellBounds3);
										e.Graphics.DrawString("△", this.Font, solidBrush, (float)e.CellBounds.X, (float)e.CellBounds.Y);
									}
									e.PaintContent(e.CellBounds);
									e.Handled = true;
								}
								else
								{
									Rectangle cellBounds4 = e.CellBounds;
									cellBounds4.Offset(new Point(-1, -1));
									e.Graphics.DrawRectangle(pen, cellBounds4);
								}
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}
	}
}
