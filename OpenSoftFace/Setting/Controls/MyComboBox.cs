using CommonConstant;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Controls
{
	internal class MyComboBox : ComboBox
	{
		private Color _borderColor = ColorUtil.Black;

		private ToolTip ttip = new ToolTip();

		[Category("Appearance"), Description("边框的颜色")]
		public Color BorderColor
		{
			get
			{
				return this._borderColor;
			}
			set
			{
				this._borderColor = value;
				base.Invalidate();
			}
		}

		public MyComboBox()
		{
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer, true);
			base.UpdateStyles();
		}

		protected override void OnDropDown(EventArgs e)
		{
			this.AdjustComboBoxDropDownListWidth();
			base.OnDropDown(e);
		}

		private void AdjustComboBoxDropDownListWidth()
		{
			try
			{
				bool flag = base.IntegralHeight && base.Items != null && base.Items.Count > 0;
				if (flag)
				{
					bool flag2 = base.Items.Count > 10;
					if (flag2)
					{
						base.IntegralHeight = false;
					}
					else
					{
						bool integralHeight = base.IntegralHeight;
						if (integralHeight)
						{
							int num = base.DropDownWidth;
							foreach (object current in base.Items)
							{
								int width = TextRenderer.MeasureText(base.GetItemText(current).ToString(), this.Font).Width;
								num = ((num < width) ? width : num);
							}
							base.DropDownWidth = num;
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		protected override void OnMouseHover(EventArgs e)
		{
			base.OnMouseHover(e);
			this.ttip.SetToolTip(this, this.Text);
		}

		protected override void WndProc(ref Message m)
		{
			try
			{
				base.WndProc(ref m);
				bool flag = m.Msg == 15 || m.Msg == 307;
				if (flag)
				{
					IntPtr windowDC = Win32.GetWindowDC(m.HWnd);
					bool flag2 = windowDC.ToInt32() == 0;
					if (!flag2)
					{
						Graphics graphics = Graphics.FromHdc(windowDC);
						graphics.SmoothingMode = SmoothingMode.AntiAlias;
						ControlPaint.DrawBorder(graphics, new Rectangle(0, 0, base.Width, base.Height), this._borderColor, ButtonBorderStyle.Solid);
						ControlPaint.DrawBorder(graphics, new Rectangle(base.Width - base.Height + 6, 0, base.Height - 6, base.Height), this._borderColor, ButtonBorderStyle.Solid);
						m.Result = IntPtr.Zero;
						Win32.ReleaseDC(m.HWnd, windowDC);
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}
	}
}
