using OpenSoftFace.Setting.Forms;
using CommonConstant;
using DAL;
using LogUtil;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace OpenSoftFace.Setting
{
	internal static class ProgramX
	{
		[DllImport("User32.dll")]
		private static extern bool ShowWindowAsync(IntPtr hWnd, int cmdShow);

		[DllImport("User32.dll")]
		private static extern bool SetForegroundWindow(IntPtr hWnd);

		[DllImport("User32.dll")]
		private static extern void SwitchToThisWindow(IntPtr hWnd, bool bRestore);

		[STAThread]
		private static void MainX()
		{
			try
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Process[] processesByName = Process.GetProcessesByName("ArcOfficeSetting");
				bool flag = processesByName == null || processesByName.Length <= 1;
				if (flag)
				{
					//Program.createDB();
					Application.Run(new LoginForm());
				}
				else
				{
					//Program.ShowWindowAsync(processesByName[0].MainWindowHandle, 5);
					//Program.SwitchToThisWindow(processesByName[0].MainWindowHandle, true);
					//Program.SetForegroundWindow(processesByName[0].MainWindowHandle);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(Program), se);
			}
		}

		private static void createDB()
		{
			bool flag = !Directory.Exists(DBConstant.DB_DIR);
			if (flag)
			{
				Directory.CreateDirectory(DBConstant.DB_DIR);
			}
			bool flag2 = !File.Exists(DBConstant.DB_DIR + "generalattendance.db");
			if (flag2)
			{
				DBHelper.CreateDatabase();
			}
			DBHelper.CreateTables();
		}
	}
}
