using CommonConstant;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Controls
{
	public class MyBorderTextBoxcs : UserControl
	{
		private string _emptyText = null;

		private Color _emptyColor = ColorUtil.DarkGray;

		private Color _borderColor = ColorUtil.SoftGray;

		private int _borderWidth = 1;

		private int paddingLeft = 5;

		private Image _backImage = null;

		private bool _isPassWord = false;

		private bool _isAllowInput = true;

		private IContainer components = null;

		public MyTextBox myTextBox;

		[Category("Appearance"), Description("是否显示密码")]
		public bool IsPassWord
		{
			get
			{
				return this._isPassWord;
			}
			set
			{
				this._isPassWord = value;
				bool isPassWord = this._isPassWord;
				if (isPassWord)
				{
					this.myTextBox.PasswordChar = '*';
				}
				base.Invalidate();
			}
		}

		[Category("Appearance"), Description("empty时显示的文本")]
		public string EmptyText
		{
			get
			{
				return this._emptyText;
			}
			set
			{
				this._emptyText = value;
				this.myTextBox.EmptyTextTip = this._emptyText;
				base.Invalidate();
			}
		}

		[Category("Appearance"), Description("empty时显示的文本颜色")]
		public Color EmptyColor
		{
			get
			{
				return this._emptyColor;
			}
			set
			{
				this._emptyColor = value;
				this.myTextBox.EmptyTextTipColor = this._emptyColor;
				base.Invalidate();
			}
		}

		[Category("Appearance"), Description("组件的边框颜色")]
		public Color BorderColor
		{
			get
			{
				return this._borderColor;
			}
			set
			{
				this._borderColor = value;
				base.Invalidate();
			}
		}

		[Category("Appearance"), Description("组件的边框宽度")]
		public int BorderWidth
		{
			get
			{
				return this._borderWidth;
			}
			set
			{
				this._borderWidth = value;
				base.Invalidate();
			}
		}

		[Category("text"), Description("文本字体大小")]
		public float FontSize
		{
			get
			{
				return this.myTextBox.Font.Size;
			}
			set
			{
				this.myTextBox.Font = new Font(this.myTextBox.Font.FontFamily, value, this.myTextBox.Font.Style);
				base.Invalidate();
			}
		}

		[Category("text"), Description("文本")]
		public override string Text
		{
			get
			{
				return this.myTextBox.Text;
			}
			set
			{
				this.myTextBox.Text = value;
				base.Invalidate();
			}
		}

		[Category("multiline"), Description("多行文本")]
		public bool Multiline
		{
			get
			{
				return this.myTextBox.Multiline;
			}
			set
			{
				this.myTextBox.Multiline = value;
				base.Invalidate();
			}
		}

		[Category("maxLength"), Description("最大字符数")]
		public int MaxLength
		{
			get
			{
				return this.myTextBox.MaxLength;
			}
			set
			{
				this.myTextBox.MaxLength = value;
				base.Invalidate();
			}
		}

		[Category("allowInput"), DefaultValue(true), Description("允许输入")]
		public bool AllowInput
		{
			get
			{
				return this._isAllowInput;
			}
			set
			{
				this._isAllowInput = value;
			}
		}

		[Category("image"), Description("图片")]
		public Image BackImage
		{
			get
			{
				return this._backImage;
			}
			set
			{
				this._backImage = value;
				bool flag = this._backImage != null;
				if (flag)
				{
					this.paddingLeft = this._backImage.Width + 20;
				}
				base.Invalidate();
			}
		}

		public MyBorderTextBoxcs()
		{
			this.InitializeComponent();
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
			this.myTextBox.KeyPress += new KeyPressEventHandler(this.MyTextBox_KeyPress);
			this.myTextBox.KeyDown += new KeyEventHandler(this.MyTextBox_KeyDown);
		}

		private void MyTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			bool flag = !this._isAllowInput;
			if (flag)
			{
				e.Handled = true;
			}
		}

		private void MyTextBox_KeyPress(object sender, KeyPressEventArgs e)
		{
			bool flag = !this._isAllowInput;
			if (flag)
			{
				e.Handled = true;
			}
		}

		private void MyBorderTextBoxcs_Resize(object sender, EventArgs e)
		{
			int height = base.Height;
			float size = this.myTextBox.Font.Size;
			int top = height - (int)size * 2 >> 1;
			int bottom = 0;
			bool multiline = this.myTextBox.Multiline;
			if (multiline)
			{
				top = 5;
				bottom = 5;
			}
			base.Padding = new Padding(this.paddingLeft, top, 5, bottom);
		}

		private void MyBorderTextBoxcs_Paint(object sender, PaintEventArgs e)
		{
			bool flag = base.BorderStyle == BorderStyle.FixedSingle;
			if (flag)
			{
				IntPtr windowDC = Win32.GetWindowDC(base.Handle);
				Graphics graphics = Graphics.FromHdc(windowDC);
				ControlPaint.DrawBorder(graphics, new Rectangle(0, 0, base.Width, base.Height), this._borderColor, this._borderWidth, ButtonBorderStyle.Solid, this._borderColor, this._borderWidth, ButtonBorderStyle.Solid, this._borderColor, this._borderWidth, ButtonBorderStyle.Solid, this._borderColor, this._borderWidth, ButtonBorderStyle.Solid);
				graphics.Dispose();
				Win32.ReleaseDC(base.Handle, windowDC);
			}
			bool flag2 = this._backImage != null;
			if (flag2)
			{
				int y = base.Height - this._backImage.Height >> 1;
				e.Graphics.DrawImage(this._backImage, new Point(10, y));
			}
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.myTextBox = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.SuspendLayout();
            // 
            // myTextBox
            // 
            this.myTextBox.BorderColor = System.Drawing.Color.Black;
            this.myTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.myTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myTextBox.EmptyTextTip = null;
            this.myTextBox.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.myTextBox.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.myTextBox.Location = new System.Drawing.Point(0, 0);
            this.myTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.myTextBox.Name = "myTextBox";
            this.myTextBox.Size = new System.Drawing.Size(350, 16);
            this.myTextBox.TabIndex = 0;
            // 
            // MyBorderTextBoxcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.myTextBox);
            this.DoubleBuffered = true;
            this.Name = "MyBorderTextBoxcs";
            this.Size = new System.Drawing.Size(350, 29);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MyBorderTextBoxcs_Paint);
            this.Resize += new System.EventHandler(this.MyBorderTextBoxcs_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
	}
}
