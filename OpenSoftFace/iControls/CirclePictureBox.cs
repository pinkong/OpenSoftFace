﻿using AForge.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenSoftFace.iControls
{
    public partial class CirclePictureBox : System.Windows.Forms.PictureBox
    {
        public CirclePictureBox()
        {
            Circle = true;
            //InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        //圆形大小随控件大小变化
        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
        {
            if (width > 0 && height > 0)
            {
                CircleSize = new Size(width, height);
            }
            base.SetBoundsCore(x, y, width, height, specified);
        }

        bool _circle;
        [Description(" 获取或设置按钮椭圆效果。"), DefaultValue(true)]
        public bool Circle
        {
            get
            {
                return _circle;
            }

            set
            {
                if (value)
                {
                    GraphicsPath gp = new GraphicsPath();
                    gp.AddEllipse(0, 0, _circleSize.Width, _circleSize.Height);//圆形
                    this.Region = new Region(gp);
                    this.BorderStyle = BorderStyle.None;
                    this.Invalidate();
                }
                _circle = value;
            }
        }

        Size _circleSize = new Size(50, 50);
        [Description(" 圆形的大小")]
        Size CircleSize
        {
            get
            {
                return _circleSize;
            }
            set
            {
                _circleSize = value;
                Circle = true;
            }
        }
    }
}
