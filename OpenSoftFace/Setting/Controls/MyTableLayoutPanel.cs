using CommonConstant;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Controls
{
	internal class MyTableLayoutPanel : TableLayoutPanel
	{
		private Color _borderColor = ColorUtil.Black;

		private int _borderWidth = 1;

		[Category("Appearance"), Description("组件的边框颜色。")]
		public Color BorderColor
		{
			get
			{
				return this._borderColor;
			}
			set
			{
				this._borderColor = value;
				base.Invalidate();
			}
		}

		[Category("Appearance"), Description("组件的边框宽度。")]
		public int BorderWidth
		{
			get
			{
				return this._borderWidth;
			}
			set
			{
				this._borderWidth = value;
				base.Invalidate();
			}
		}

		public MyTableLayoutPanel()
		{
			base.SetStyle(ControlStyles.DoubleBuffer, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, false);
			base.SetStyle(ControlStyles.ResizeRedraw, true);
			base.SetStyle(ControlStyles.UserPaint, true);
			base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			base.Paint += new PaintEventHandler(this.MyTableLayoutPanel_Paint);
		}

		private void MyTableLayoutPanel_Paint(object sender, PaintEventArgs e)
		{
			IntPtr windowDC = Win32.GetWindowDC(base.Handle);
			Graphics graphics = Graphics.FromHdc(windowDC);
			ControlPaint.DrawBorder(graphics, new Rectangle(20, 0, base.Width - 40, base.Height), this._borderColor, 0, ButtonBorderStyle.Solid, this._borderColor, 0, ButtonBorderStyle.Solid, this._borderColor, 0, ButtonBorderStyle.Solid, this._borderColor, 0, ButtonBorderStyle.Solid);
			graphics.Dispose();
			Win32.ReleaseDC(base.Handle, windowDC);
		}
	}
}
