using System;

namespace Model.Entity
{
	public class AttendanceRecord
	{
		public int ID
		{
			get;
			set;
		}

		public int PersonId
		{
			get;
			set;
		}

		public string PersonSerial
		{
			get;
			set;
		}

		public string Date
		{
			get;
			set;
		}

		public string StartTime
		{
			get;
			set;
		}

		public string EndTime
		{
			get;
			set;
		}

		public string StartAttendUrl
		{
			get;
			set;
		}

		public string EndAttendUrl
		{
			get;
			set;
		}

		public int AttendStatus
		{
			get;
			set;
		}

		public string AddTime
		{
			get;
			set;
		}

		public AttendanceRecord()
		{
			this.AttendStatus = 0;
		}
	}
}
