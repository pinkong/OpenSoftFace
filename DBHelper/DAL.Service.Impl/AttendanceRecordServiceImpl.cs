using CommonConstant;
using LogUtil;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using Utils;

namespace DAL.Service.Impl
{
	public class AttendanceRecordServiceImpl : IAttendanceRecordService
	{
		private string connectionString;

		public AttendanceRecordServiceImpl()
		{
			try
			{
				this.connectionString = new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "CheckRecord.db"
				}.ToString();
			}
			catch (Exception ex)
			{
				LogHelper.LogError(base.GetType(), ex);
			}
		}

		public int Count(string[] where = null, string groupBy = null, bool distinct = false)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select distinct count(*) as num from {0} ", "AttendRecord"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(" where ");
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					if (groupBy != null && groupBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" group by {0} ", groupBy));
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						int num = 0;
						if (reader.Read())
						{
							num = int.Parse(reader["num"].ToString());
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						return num;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public int Delete(int recordID)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("delete from {0} where id={1}", "AttendRecord", recordID);
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_72_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_72_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public AttendanceRecord Get(int recordID)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = string.Format("select * from {0} where id={1}", "AttendRecord", recordID);
					Console.WriteLine("sql:{0}", sql);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sql;
						SQLiteDataReader reader = command.ExecuteReader();
						AttendanceRecord record = null;
						if (reader.Read())
						{
							record = new AttendanceRecord();
							record.ID = StringUtil.CheckSqlIntValue(reader["id"].ToString());
							record.PersonId = StringUtil.CheckSqlIntValue(reader["person_id"].ToString());
							record.PersonSerial = reader["person_serial"].ToString();
							record.Date = reader["date"].ToString();
							record.StartTime = reader["start_time"].ToString();
							record.EndTime = reader["end_time"].ToString();
							record.StartAttendUrl = reader["start_attend_url"].ToString();
							record.EndAttendUrl = reader["end_attend_url"].ToString();
							record.AttendStatus = StringUtil.CheckSqlIntValue(reader["attend_status"].ToString());
							record.AddTime = reader["add_time"].ToString();
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						return record;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return null;
		}

		public int Insert(AttendanceRecord record)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("insert into {0} values(null,{1},'{2}','{3}','{4}','{5}','{6}','{7}',{8},'{9}')", new object[]
					{
						"AttendRecord",
						record.PersonId,
						record.PersonSerial,
						record.Date,
						record.StartTime,
						record.EndTime,
						record.StartAttendUrl,
						record.EndAttendUrl,
						record.AttendStatus,
						record.AddTime
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_D2_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_D2_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public List<AttendanceRecord> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1)
		{
			List<AttendanceRecord> list = new List<AttendanceRecord>();
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select * from {0} ", "AttendRecord"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(string.Format(" where ", new object[0]));
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					if (groupBy != null && groupBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" group by {0} ", groupBy));
					}
					if (having != null && having != string.Empty)
					{
						sqlBuilder.Append(string.Format(" having {0} ", having));
					}
					if (orderBy != null && orderBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" order by {0} ", orderBy));
					}
					if (offset >= 0 && size >= 0)
					{
						sqlBuilder.Append(string.Format(" limit {0},{1} ", offset, size));
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						while (reader.Read())
						{
							list.Add(new AttendanceRecord
							{
								ID = StringUtil.CheckSqlIntValue(reader["id"].ToString()),
								PersonId = StringUtil.CheckSqlIntValue(reader["person_id"].ToString()),
								PersonSerial = reader["person_serial"].ToString(),
								Date = reader["date"].ToString(),
								StartTime = reader["start_time"].ToString(),
								EndTime = reader["end_time"].ToString(),
								StartAttendUrl = reader["start_attend_url"].ToString(),
								EndAttendUrl = reader["end_attend_url"].ToString(),
								AttendStatus = StringUtil.CheckSqlIntValue(reader["attend_status"].ToString()),
								AddTime = reader["add_time"].ToString()
							});
						}
						reader.Close();
						command.Dispose();
						conn.Close();
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return list;
		}

		public int Update(AttendanceRecord record)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("update {0} set person_id={1},person_serial='{2}',date='{3}',start_time='{4}',end_time='{5}',start_attend_url='{6}',end_attend_url='{7}',attend_status={8},add_time='{9}' where id={10}", new object[]
					{
						"AttendRecord",
						record.PersonId,
						record.PersonSerial,
						record.Date,
						record.StartTime,
						record.EndTime,
						record.StartAttendUrl,
						record.EndAttendUrl,
						record.AttendStatus,
						record.AddTime,
						record.ID
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_E1_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_E1_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public Dictionary<string, AttendanceRecord2> QueryForAttendRecord(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1)
		{
			Dictionary<string, AttendanceRecord2> dict = new Dictionary<string, AttendanceRecord2>();
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select id,person_id,person_serial,date,start_time,end_time,start_attend_url,end_attend_url,attend_status,add_time from {0} ", "AttendRecord"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(string.Format(" where ", new object[0]));
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					if (groupBy != null && groupBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" group by {0} ", groupBy));
					}
					if (having != null && having != string.Empty)
					{
						sqlBuilder.Append(string.Format(" having {0} ", having));
					}
					if (orderBy != null && orderBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" order by {0} ", orderBy));
					}
					if (offset >= 0 && size >= 0)
					{
						sqlBuilder.Append(string.Format(" limit {0},{1} ", offset, size));
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						while (reader.Read())
						{
							AttendanceRecord2 record = new AttendanceRecord2();
							record.ID = StringUtil.CheckSqlIntValue(reader["id"].ToString());
							record.PersonId = StringUtil.CheckSqlIntValue(reader["person_id"].ToString());
							record.PersonSerial = reader["person_serial"].ToString();
							record.Date = reader["date"].ToString();
							record.StartTime = reader["start_time"].ToString();
							record.EndTime = reader["end_time"].ToString();
							record.StartAttendUrl = reader["start_attend_url"].ToString();
							record.EndAttendUrl = reader["end_attend_url"].ToString();
							record.AttendStatus = StringUtil.CheckSqlIntValue(reader["attend_status"].ToString());
							if (!string.IsNullOrWhiteSpace(record.StartTime) && !string.IsNullOrWhiteSpace(record.Date))
							{
								record.StartTime = record.StartTime.Replace(record.Date, string.Empty).Trim();
							}
							if (!string.IsNullOrWhiteSpace(record.EndTime) && !string.IsNullOrWhiteSpace(record.Date))
							{
								record.EndTime = record.EndTime.Replace(record.Date, string.Empty).Trim();
							}
							if (!string.IsNullOrWhiteSpace(record.Date))
							{
								string key = string.Format("{0}${1}", record.PersonId, record.Date.Replace("-", "/"));
								if (!dict.ContainsKey(key))
								{
									dict.Add(key, record);
								}
							}
						}
						reader.Close();
						command.Dispose();
						conn.Close();
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return dict;
		}
	}
}
