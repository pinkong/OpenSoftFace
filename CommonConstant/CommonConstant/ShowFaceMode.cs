using System;

namespace CommonConstant
{
	public class ShowFaceMode
	{
		public const string SINGLE_FACE_MODE = "1";

		public const string MULTIP_FACE_MODE = "2";
	}
}
