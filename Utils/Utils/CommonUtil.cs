using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace Utils
{
	public class CommonUtil
	{
		public static T DeepCopyWithBinarySerialize<T>(T obj)
		{
			object obj2;
			try
			{
				using (MemoryStream memoryStream = new MemoryStream())
				{
					BinaryFormatter expr_0B = new BinaryFormatter();
					expr_0B.Serialize(memoryStream, obj);
					memoryStream.Seek(0L, SeekOrigin.Begin);
					obj2 = expr_0B.Deserialize(memoryStream);
					memoryStream.Close();
				}
			}
			catch
			{
				T t = default(T);
				t = t;
				return t;
			}
			return (T)((object)obj2);
		}

		public static object CopyOjbect(object obj)
		{
			if (obj == null)
			{
				return null;
			}
			Type type = obj.GetType();
			object obj2;
			if (type.IsValueType || type.FullName == "System.String")
			{
				obj2 = obj;
			}
			else
			{
				obj2 = Activator.CreateInstance(type);
				MemberInfo[] members = obj.GetType().GetMembers();
				for (int i = 0; i < members.Length; i++)
				{
					MemberInfo memberInfo = members[i];
					if (memberInfo.MemberType == MemberTypes.Field)
					{
						FieldInfo fieldInfo = (FieldInfo)memberInfo;
						object value = fieldInfo.GetValue(obj);
						if (value is ICloneable)
						{
							fieldInfo.SetValue(obj2, (value as ICloneable).Clone());
						}
						else
						{
							fieldInfo.SetValue(obj2, CommonUtil.CopyOjbect(value));
						}
					}
					else if (memberInfo.MemberType == MemberTypes.Property)
					{
						PropertyInfo propertyInfo = (PropertyInfo)memberInfo;
						if (propertyInfo.GetSetMethod(false) != null)
						{
							try
							{
								object value2 = propertyInfo.GetValue(obj, null);
								if (value2 is ICloneable)
								{
									propertyInfo.SetValue(obj2, (value2 as ICloneable).Clone(), null);
								}
								else
								{
									propertyInfo.SetValue(obj2, CommonUtil.CopyOjbect(value2), null);
								}
							}
							catch
							{
							}
						}
					}
				}
			}
			return obj2;
		}
	}
}
