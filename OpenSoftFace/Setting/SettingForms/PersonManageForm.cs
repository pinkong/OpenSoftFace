using OpenSoftFace.Setting.Common;
using OpenSoftFace.Setting.Controls;
using DAL.Service;
using DAL.Service.Impl;
using LogUtil;
using Model.Common;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Forms.SettingForms
{
	public class PersonManageForm : Form
	{
		private IMainInfoService personInfoService = new MainInfoServiceImpl();

		private IFeatureInfoService faceInfoService = new FeatureInfoServiceImpl();

		private StringBuilder sqlBuilder = new StringBuilder();

		private const string orderBy = " id desc ";

		private List<MainInfo> recordList;

		private int offset;

		private string dbtEditName = "detail";

		private string savePath = string.Empty;

		private IniHelper iniHelper = new IniHelper("setting.ini", "setting");

		private string queryName = "";

		private BaseForm baseForm;

		private IContainer components = null;

		private SplitContainer contriner_right;

		private MyTableLayoutPanel tablePanel;

		private MyButton btn_delete;

		private MyButton btn_add;

		private MySplitDataGridView personDataGridView;

		private MyBoderPanel boderPanel;

		private MyTextBox txt_name;

		private PictureBox btn_search;

		public PersonManageForm(BaseForm _baseForm)
		{
			this.InitializeComponent();
			this.baseForm = _baseForm;
			base.TopMost = true;
			this.loadSavePath();
		}

		private void loadSavePath()
		{
			try
			{
				bool flag = this.iniHelper.Dict.ContainsKey("save_path");
				if (flag)
				{
					this.savePath = this.iniHelper.Dict["save_path"];
					bool flag2 = !string.IsNullOrWhiteSpace(this.savePath);
					if (flag2)
					{
						this.savePath = this.savePath.Replace("\0", string.Empty);
						bool flag3 = !Directory.Exists(this.savePath);
						if (flag3)
						{
							Directory.CreateDirectory(this.savePath);
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void PersonManageForm_Load(object sender, EventArgs e)
		{
			try
			{
				base.SuspendLayout();
				this.personDataGridView.PageSize = 10;
				Dictionary<string, int> dictValue = new Dictionary<string, int>
				{
					{
						"姓名",
						0
					},
					{
						"编号",
						0
					},
					{
						"部门",
						0
					},
					{
						"添加时间",
						0
					}
				};
				this.personDataGridView.CreateListHeader(dictValue, 60);
				DataGridViewTextBoxColumn dataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
				dataGridViewTextBoxColumn.CellTemplate.Value = "编辑";
				dataGridViewTextBoxColumn.Name = this.dbtEditName;
				dataGridViewTextBoxColumn.HeaderText = "操作";
				dataGridViewTextBoxColumn.DefaultCellStyle.NullValue = "编辑";
				dataGridViewTextBoxColumn.Width = 60;
				dataGridViewTextBoxColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
				this.personDataGridView.AddButton(dataGridViewTextBoxColumn, DataGridViewAutoSizeColumnMode.None);
				this.InitDataGridView();
				int y = this.boderPanel.Height - this.txt_name.Height >> 1;
				this.txt_name.Location = new Point(0, y);
				base.ResumeLayout();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void InitDataGridView()
		{
			try
			{
				string[] array = new string[1];
				string value = "p_valid != -1";
				bool flag = this.sqlBuilder.ToString().IndexOf(value) == -1;
				if (flag)
				{
					this.sqlBuilder.Append(value);
				}
				array[0] = this.sqlBuilder.ToString();
				this.recordList = this.personInfoService.QueryForList(array, null, null, " id desc ", 0, this.personDataGridView.PageSize);
				this.addDataGridItem(this.recordList, false);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void addDataGridItem(List<MainInfo> personList, bool isOffset)
		{
			try
			{
				this.personDataGridView.ClearDataGridView();
				string[] where = new string[5];
				string value = "p_valid != -1";
				bool flag = string.IsNullOrEmpty(this.sqlBuilder.ToString());
				if (flag)
				{
					this.sqlBuilder.Append(value);
				}
				else
				{
					bool flag2 = this.sqlBuilder.ToString().IndexOf(value) == -1;
					if (flag2)
					{
						this.sqlBuilder.Append(",").Append(value);
					}
				}
				where = this.sqlBuilder.ToString().Split(new char[]
				{
					','
				});
				int allSize = this.personInfoService.Count(where);
				this.personDataGridView.AllSize = allSize;
				int count = personList.Count;
				this.personDataGridView.Calculation(isOffset);
				for (int i = 0; i < count; i++)
				{
					DateTime now = DateTime.Now;
					string text = personList[i].AddTime;
					bool flag3 = DateTime.TryParse(personList[i].AddTime, out now);
					if (flag3)
					{
						text = now.ToString("yyyy/MM/dd", DateTimeFormatInfo.InvariantInfo);
					}
					string[] value2 = new string[]
					{
						StringUtil.CheckStringEmpty(personList[i].PersonName),
						StringUtil.CheckStringEmpty(personList[i].PersonSerial),
						StringUtil.CheckStringEmpty(personList[i].Dept),
						text
					};
					this.personDataGridView.AddRowData(value2, personList[i].ID.ToString(), string.Concat((this.personDataGridView.PageIndex - 1) * this.personDataGridView.PageSize + i + 1));
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void mySplitListView1_UserControlBtnClicked(object sender, MyEventArgs e)
		{
			this.offset = e.Value;
			this.initData(e.Value, false);
		}

		public void initData(int offset, bool isReset)
		{
			try
			{
				string[] where = new string[5];
				where = (string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				}));
				this.recordList = this.personInfoService.QueryForList(where, null, null, " id desc ", offset, this.personDataGridView.PageSize);
				this.addDataGridItem(this.recordList, isReset);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void search_btn_Click(object sender, EventArgs e)
		{
			try
			{
				this.queryName = this.txt_name.Text.Trim();
				string msg = "";
				bool flag = this.queryName.Length > 0;
				if (flag)
				{
					bool flag2 = StringUtil.CommonCheckString(this.queryName, "姓名或编号", 10, out msg) > 0;
					if (flag2)
					{
						ErrorDialogForm.ShowErrorMsg(msg, "", false, null, false);
						return;
					}
				}
				string[] where = new string[2];
				this.personDataGridView.cancelAllSelect();
				this.sqlBuilder.Clear();
				this.sqlBuilder.Append(string.Format("(person_name like '%{0}%' or person_serial like '%{0}%'),p_valid != -1", this.txt_name.Text));
				where = (string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				}));
				this.offset = 0;
				this.recordList = this.personInfoService.QueryForList(where, null, null, " id desc ", 0, this.personDataGridView.PageSize);
				this.addDataGridItem(this.recordList, true);
				this.personDataGridView.cancelAllSelect();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void delete_btn_Click(object sender, EventArgs e)
		{
			try
			{
				List<int> checkedItemIndex = this.personDataGridView.GetCheckedItemIndex();
				bool flag = false;
				bool flag2 = this.personDataGridView.AllSelectBtnShowState && "取消全选".Equals(this.personDataGridView.AllSelectBtnText);
				if (flag2)
				{
					flag = true;
					this.updateRecordList();
					bool flag3 = this.recordList == null || this.recordList.Count <= 0;
					if (flag3)
					{
						ErrorDialogForm.ShowErrorMsg("请选择需要删除的人员!", "", false, null, false);
						return;
					}
				}
				else
				{
					int count = checkedItemIndex.Count;
					bool flag4 = count == 0;
					if (flag4)
					{
						ErrorDialogForm.ShowErrorMsg("请选择需要删除的人员!", "", false, null, false);
						return;
					}
				}
				DialogResult dialogResult = WarningDialogForm.showWarningMsg("确认删除吗?", "", false, null);
				bool flag5 = dialogResult == DialogResult.OK;
				if (flag5)
				{
					IList<MainInfo> list = new List<MainInfo>();
					List<int> list2 = new List<int>();
					bool flag6 = flag;
					if (flag6)
					{
						for (int i = 0; i < this.recordList.Count; i++)
						{
							MainInfo mainInfo = this.recordList[i];
							list2.Add(mainInfo.ID);
							list.Add(mainInfo);
						}
					}
					else
					{
						foreach (int current in checkedItemIndex)
						{
							MainInfo mainInfo2 = this.recordList[current];
							list2.Add(mainInfo2.ID);
							list.Add(mainInfo2);
						}
					}
					bool flag7 = list2 != null && list2.Count > 0;
					if (flag7)
					{
						int num = this.personInfoService.Update(list2, -1);
						bool flag8 = num > 0;
						if (flag8)
						{
							this.faceInfoService.Update(list2, -1);
						}
					}
				}
				this.personDataGridView.CheckBoxSelect(false);
				this.offset = 0;
				this.initData(0, true);
				this.personDataGridView.cancelAllSelect();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void updateRecordList()
		{
			try
			{
				string text = this.txt_name.Text.Trim();
				string msg = "";
				bool flag = text.Length > 0;
				if (flag)
				{
					bool flag2 = StringUtil.CommonCheckString(text, "姓名或编号", 50, out msg) > 0;
					if (flag2)
					{
						ErrorDialogForm.ShowErrorMsg(msg, "", false, null, false);
						return;
					}
				}
				string[] where = new string[2];
				this.sqlBuilder.Clear();
				bool flag3 = string.IsNullOrWhiteSpace(text);
				if (flag3)
				{
					this.sqlBuilder.Append(string.Format("p_valid != -1", text));
				}
				else
				{
					this.sqlBuilder.Append(string.Format("(person_name like '%{0}%' or person_serial like '%{0}%'),p_valid != -1", text));
				}
				where = (string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				}));
				this.recordList = this.personInfoService.QueryForList(where, null, null, " id desc ", -1, -1);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void myTextBox1_KeyDown(object sender, KeyEventArgs e)
		{
			bool flag = e.KeyCode == Keys.Return;
			if (flag)
			{
				this.search_btn_Click(sender, e);
			}
		}

		private void mySplitDataGridView1_dataGridViewCheckClicked(object sender, MyEventArgs e)
		{
			try
			{
				DataGridView dataGridView = (DataGridView)sender;
				bool flag = this.dbtEditName.Equals(this.personDataGridView.Columns[dataGridView.CurrentCell.OwningColumn.Index].Name);
				if (flag)
				{
					PersonAddForm personAddForm = new PersonAddForm(int.Parse(e.StrId), this.savePath);
					personAddForm.FormClosed += delegate
					{
						this.personDataGridView.cancelAllSelect();
						bool flag3 = string.IsNullOrEmpty(this.queryName);
						bool flag4 = !flag3;
						if (flag4)
						{
							this.offset = 0;
						}
						this.initData(this.offset, !flag3);
						this.baseForm.HideOpaqueLayer();
						this.baseForm.TopMost = true;
					};
					this.baseForm.ShowOpaqueLayer();
					personAddForm.ShowDialog();
				}
				else
				{
					bool flag2 = "取消全选".Equals(this.personDataGridView.AllSelectBtnText);
					if (flag2)
					{
						this.personDataGridView.AllSelectBtnText = "全选";
						this.personDataGridView.AllSelectBtnCheckState = false;
						this.personDataGridView.CancelSelectedForHeader(false);
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void myBoderPanel1_Resize(object sender, EventArgs e)
		{
			this.txt_name.Location = new Point(0, this.boderPanel.Height - this.txt_name.Height >> 1);
		}

		private void btn_add_Click(object sender, EventArgs e)
		{
			try
			{
				PersonAddForm personAddForm = new PersonAddForm(this.savePath);
				personAddForm.FormClosed += delegate
				{
					this.offset = 0;
					this.initData(0, true);
					this.personDataGridView.CancelSelectedForHeader(false);
					this.personDataGridView.CancelAllSelect();
					this.baseForm.HideOpaqueLayer();
				};
				this.baseForm.ShowOpaqueLayer();
				personAddForm.ShowDialog();
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.contriner_right = new System.Windows.Forms.SplitContainer();
            this.tablePanel = new OpenSoftFace.Setting.Controls.MyTableLayoutPanel();
            this.btn_delete = new OpenSoftFace.Setting.Controls.MyButton();
            this.boderPanel = new OpenSoftFace.Setting.Controls.MyBoderPanel();
            this.txt_name = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.btn_search = new System.Windows.Forms.PictureBox();
            this.btn_add = new OpenSoftFace.Setting.Controls.MyButton();
            this.personDataGridView = new OpenSoftFace.Setting.Controls.MySplitDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.contriner_right)).BeginInit();
            this.contriner_right.Panel1.SuspendLayout();
            this.contriner_right.Panel2.SuspendLayout();
            this.contriner_right.SuspendLayout();
            this.tablePanel.SuspendLayout();
            this.boderPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_search)).BeginInit();
            this.SuspendLayout();
            // 
            // contriner_right
            // 
            this.contriner_right.BackColor = System.Drawing.Color.White;
            this.contriner_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contriner_right.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.contriner_right.IsSplitterFixed = true;
            this.contriner_right.Location = new System.Drawing.Point(5, 5);
            this.contriner_right.Margin = new System.Windows.Forms.Padding(0);
            this.contriner_right.Name = "contriner_right";
            this.contriner_right.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // contriner_right.Panel1
            // 
            this.contriner_right.Panel1.Controls.Add(this.tablePanel);
            this.contriner_right.Panel1MinSize = 40;
            // 
            // contriner_right.Panel2
            // 
            this.contriner_right.Panel2.Controls.Add(this.personDataGridView);
            this.contriner_right.Panel2.Padding = new System.Windows.Forms.Padding(7, 0, 7, 10);
            this.contriner_right.Size = new System.Drawing.Size(659, 480);
            this.contriner_right.TabIndex = 1;
            // 
            // tablePanel
            // 
            this.tablePanel.BorderColor = System.Drawing.Color.Black;
            this.tablePanel.BorderWidth = 1;
            this.tablePanel.ColumnCount = 4;
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 270F));
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tablePanel.Controls.Add(this.btn_delete, 3, 0);
            this.tablePanel.Controls.Add(this.boderPanel, 0, 0);
            this.tablePanel.Controls.Add(this.btn_add, 2, 0);
            this.tablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePanel.Location = new System.Drawing.Point(0, 0);
            this.tablePanel.Name = "tablePanel";
            this.tablePanel.Padding = new System.Windows.Forms.Padding(15, 3, 15, 5);
            this.tablePanel.RowCount = 1;
            this.tablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablePanel.Size = new System.Drawing.Size(659, 50);
            this.tablePanel.TabIndex = 3;
            // 
            // btn_delete
            // 
            this.btn_delete.BackColor = System.Drawing.Color.White;
            this.btn_delete.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_delete.EnterForeColor = System.Drawing.Color.White;
            this.btn_delete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_delete.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_delete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_delete.LeaveBackColor = System.Drawing.Color.White;
            this.btn_delete.LeaveForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_delete.Location = new System.Drawing.Point(572, 6);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(69, 36);
            this.btn_delete.TabIndex = 1;
            this.btn_delete.Text = "删除";
            this.btn_delete.UseVisualStyleBackColor = false;
            this.btn_delete.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // boderPanel
            // 
            this.boderPanel.BackColor = System.Drawing.Color.White;
            this.boderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.boderPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.boderPanel.BorderWidth = 1;
            this.boderPanel.Controls.Add(this.txt_name);
            this.boderPanel.Controls.Add(this.btn_search);
            this.boderPanel.Location = new System.Drawing.Point(18, 6);
            this.boderPanel.Name = "boderPanel";
            this.boderPanel.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.boderPanel.Size = new System.Drawing.Size(264, 36);
            this.boderPanel.TabIndex = 0;
            // 
            // txt_name
            // 
            this.txt_name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txt_name.BorderColor = System.Drawing.Color.Black;
            this.txt_name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_name.EmptyTextTip = "请输入姓名或编号进行搜索";
            this.txt_name.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_name.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.txt_name.Location = new System.Drawing.Point(-1, 3);
            this.txt_name.MaxLength = 20;
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(185, 18);
            this.txt_name.TabIndex = 2;
            this.txt_name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.myTextBox1_KeyDown);
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.Color.White;
            this.btn_search.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn_search.Location = new System.Drawing.Point(227, 0);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(35, 34);
            this.btn_search.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btn_search.TabIndex = 1;
            this.btn_search.TabStop = false;
            this.btn_search.Click += new System.EventHandler(this.search_btn_Click);
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.White;
            this.btn_add.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_add.EnterForeColor = System.Drawing.Color.White;
            this.btn_add.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_add.LeaveBackColor = System.Drawing.Color.White;
            this.btn_add.LeaveForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_add.Location = new System.Drawing.Point(497, 6);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(69, 36);
            this.btn_add.TabIndex = 0;
            this.btn_add.Text = "增加";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // personDataGridView
            // 
            this.personDataGridView.AllSelectBtnCheckState = false;
            this.personDataGridView.AllSelectBtnShowState = false;
            this.personDataGridView.AllSelectBtnText = "全选";
            this.personDataGridView.AllSize = 0;
            this.personDataGridView.AutoScroll = true;
            this.personDataGridView.BackColor = System.Drawing.Color.White;
            this.personDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.personDataGridView.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.personDataGridView.IsCheckMouseState = false;
            this.personDataGridView.Location = new System.Drawing.Point(7, 0);
            this.personDataGridView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.personDataGridView.Name = "personDataGridView";
            this.personDataGridView.Offset = 0;
            this.personDataGridView.PageIndex = 1;
            this.personDataGridView.PageSize = 2;
            this.personDataGridView.RefershBtnShowState = false;
            this.personDataGridView.Size = new System.Drawing.Size(645, 416);
            this.personDataGridView.TabIndex = 4;
            this.personDataGridView.TabStop = false;
            this.personDataGridView.PaginationBtnClicked += new OpenSoftFace.Setting.Controls.MySplitDataGridView.PaginationBtnClickHandle(this.mySplitListView1_UserControlBtnClicked);
            this.personDataGridView.DataGridViewCheckClicked += new OpenSoftFace.Setting.Controls.MySplitDataGridView.DataGridClickHandle(this.mySplitDataGridView1_dataGridViewCheckClicked);
            this.personDataGridView.Load += new System.EventHandler(this.personDataGridView_Load);
            // 
            // PersonManageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(669, 490);
            this.Controls.Add(this.contriner_right);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PersonManageForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "PersonManageForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.PersonManageForm_Load);
            this.contriner_right.Panel1.ResumeLayout(false);
            this.contriner_right.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.contriner_right)).EndInit();
            this.contriner_right.ResumeLayout(false);
            this.tablePanel.ResumeLayout(false);
            this.boderPanel.ResumeLayout(false);
            this.boderPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_search)).EndInit();
            this.ResumeLayout(false);

		}

        private void personDataGridView_Load(object sender, EventArgs e)
        {

        }
	}
}
