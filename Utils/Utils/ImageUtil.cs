using LogUtil;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Utils
{
	public class ImageUtil
	{
		public static byte[] CompressionImage(Stream fileStream, long quality, bool isRotate)
		{
			byte[] result;
			using (Image image = Image.FromStream(fileStream))
			{
				ImageCodecInfo encoder = ImageUtil.GetEncoder(ImageFormat.Jpeg);
				if (isRotate)
				{
					image.RotateFlip(RotateFlipType.Rotate90FlipNone);
				}
				using (Bitmap bitmap = new Bitmap(image))
				{
					Encoder arg_30_0 = Encoder.Quality;
					EncoderParameters encoderParameters = new EncoderParameters(1);
					EncoderParameter encoderParameter = new EncoderParameter(arg_30_0, quality);
					encoderParameters.Param[0] = encoderParameter;
					using (MemoryStream memoryStream = new MemoryStream())
					{
						try
						{
							bitmap.Save(memoryStream, encoder, encoderParameters);
							encoderParameters.Dispose();
							encoderParameter.Dispose();
							fileStream.Dispose();
							fileStream.Close();
							bitmap.Dispose();
							image.Dispose();
						}
						catch (Exception se)
						{
							LogHelper.LogError(typeof(ImageUtil), se);
						}
						result = memoryStream.ToArray();
					}
				}
			}
			return result;
		}

		public static ImageCodecInfo GetEncoder(ImageFormat format)
		{
			ImageCodecInfo result;
			try
			{
				ImageCodecInfo[] imageDecoders = ImageCodecInfo.GetImageDecoders();
				for (int i = 0; i < imageDecoders.Length; i++)
				{
					ImageCodecInfo imageCodecInfo = imageDecoders[i];
					if (imageCodecInfo.FormatID == format.Guid)
					{
						result = imageCodecInfo;
						return result;
					}
				}
				result = null;
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(ImageUtil), se);
				result = null;
			}
			return result;
		}
	}
}
