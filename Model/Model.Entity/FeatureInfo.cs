using System;

namespace Model.Entity
{
	public class FeatureInfo
	{
		public int ID
		{
			get;
			set;
		}

		public int PersonId
		{
			get;
			set;
		}

		public int FaceId
		{
			get;
			set;
		}

		public string RegisImageName
		{
			get;
			set;
		}

		public int FLeft
		{
			get;
			set;
		}

		public int FTop
		{
			get;
			set;
		}

		public int FRight
		{
			get;
			set;
		}

		public int FBottom
		{
			get;
			set;
		}

		public int FOrient
		{
			get;
			set;
		}

		public int FValid
		{
			get;
			set;
		}

		public byte[] Feature
		{
			get;
			set;
		}

		public int FeatureSize
		{
			get;
			set;
		}

		public FeatureInfo()
		{
			this.FeatureSize = 1032;
		}
	}
}
