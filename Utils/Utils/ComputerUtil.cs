using System;
using System.Management;

namespace Utils
{
	public class ComputerUtil
	{
		public static string GetMacAddress()
		{
			string result;
			try
			{
				string text = string.Empty;
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = new ManagementClass("Win32_NetworkAdapterConfiguration").GetInstances().GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						if ((bool)managementObject["IPEnabled"])
						{
							text = managementObject["MacAddress"].ToString();
						}
					}
				}
				result = text;
			}
			catch
			{
				result = "unknown";
			}
			return result;
		}

		public static string GetMotherBoardID()
		{
			string result;
			try
			{
				ManagementObjectCollection arg_11_0 = new ManagementClass("Win32_BaseBoard").GetInstances();
				string text = null;
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = arg_11_0.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						text = ((ManagementObject)enumerator.Current).Properties["SerialNumber"].Value.ToString();
					}
				}
				result = text;
			}
			catch
			{
				result = "unknown";
			}
			return result;
		}

		public static string GetDBPasswordStr()
		{
			return "";
		}
	}
}
