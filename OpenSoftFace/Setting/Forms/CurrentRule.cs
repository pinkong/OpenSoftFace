using OpenSoftFace.Setting.Controls;
using LogUtil;
using Model.Entity;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Forms
{
	public class CurrentRule : DragBaseForm
	{
		public delegate DialogResult InvokeDelegate(Form parent, Form subForm);

		private static CurrentRule updateForm;

		private static DialogResult dialog;

		public static int labelWidth = 10;

		private IContainer components = null;

		private MyButton btn_ok;

		private Label lbl_attend_effect;

		private CheckBox cbx_sunday;

		private CheckBox cbx_saturday;

		private CheckBox cbx_friday;

		private CheckBox cbx_thursday;

		private CheckBox cbx_wednesday;

		private CheckBox cbx_tuesday;

		private CheckBox cbx_monday;

		private Panel panel_liveness;

		private MyTextBox txt_work_hour;

		private RadioButton rab_recog_mode2;

		private Label label3;

		private Label label2;

		private RadioButton rab_recog_mode1;

		private Label label1;

		private Label label4;

		private Label label7;

		private Label label5;

		private MyTextBox datetime_end;

		private MyTextBox datetime_start;

		private Panel panel_effective_mode;

		private RadioButton rab_effective_mode_2;

		private RadioButton rab_effective_mode_1;

		private Label label6;

		public CurrentRule()
		{
			this.InitializeComponent();
		}

		public CurrentRule(AttendRuleParam ruleParam)
		{
			this.InitializeComponent();
			this.fillInfoControl(ruleParam);
		}

		public void fillInfoControl(AttendRuleParam ruleParam)
		{
			try
			{
				bool flag = true;
				bool flag2 = ruleParam != null && "1".Equals(ruleParam.attend_rule_mode);
				if (flag2)
				{
					flag = false;
				}
				bool flag3 = flag;
				if (flag3)
				{
					this.rab_recog_mode1.Checked = true;
					this.rab_recog_mode2.Checked = false;
					this.txt_work_hour.Enabled = false;
				}
				else
				{
					this.rab_recog_mode1.Checked = false;
					this.rab_recog_mode2.Checked = true;
					this.datetime_start.Enabled = false;
					this.datetime_end.Enabled = false;
				}
				string text = "09:00";
				bool flag4 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.attend_start_time);
				if (flag4)
				{
					text = ruleParam.attend_start_time;
				}
				this.datetime_start.Text = text;
				string text2 = "18:00";
				bool flag5 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.attend_end_time);
				if (flag5)
				{
					text2 = ruleParam.attend_end_time;
				}
				this.datetime_end.Text = text2;
				string text3 = "8";
				bool flag6 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.work_hours_limit);
				if (flag6)
				{
					text3 = ruleParam.work_hours_limit;
				}
				this.txt_work_hour.Text = text3;
				this.lbl_attend_effect.Text = "00:00:00 - 23:59:59";
				string source = string.Empty;
				bool flag7 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.rest_day);
				if (flag7)
				{
					source = ruleParam.rest_day;
				}
				this.SetCbxCheckState(this.cbx_monday, source, "1");
				this.SetCbxCheckState(this.cbx_tuesday, source, "2");
				this.SetCbxCheckState(this.cbx_wednesday, source, "3");
				this.SetCbxCheckState(this.cbx_thursday, source, "4");
				this.SetCbxCheckState(this.cbx_friday, source, "5");
				this.SetCbxCheckState(this.cbx_saturday, source, "6");
				this.SetCbxCheckState(this.cbx_sunday, source, "7");
				int obj = 1;
				bool flag8 = ruleParam != null && ruleParam.effective_mode >= 0;
				if (flag8)
				{
					bool flag9 = 0.Equals(ruleParam.effective_mode);
					if (flag9)
					{
						obj = 0;
					}
				}
				this.rab_effective_mode_1.Checked = 1.Equals(obj);
				this.rab_effective_mode_2.Checked = 0.Equals(obj);
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void SetCbxCheckState(CheckBox cbx, string source, string key)
		{
			cbx.Checked = (!string.IsNullOrWhiteSpace(source) && source.Contains(key));
		}

		public static DialogResult showCrrentRule(AttendRuleParam ruleParam)
		{
			CurrentRule.updateForm = new CurrentRule(ruleParam);
			CurrentRule.updateForm.TopMost = true;
			CurrentRule.updateForm.ShowDialog();
			return CurrentRule.dialog;
		}

		private void ok_Click(object sender, EventArgs e)
		{
			CurrentRule.dialog = DialogResult.OK;
			CurrentRule.updateForm.DialogResult = CurrentRule.dialog;
			CurrentRule.updateForm.Close();
		}

		public override void Close_btn_Click(object sender, EventArgs e)
		{
			CurrentRule.dialog = DialogResult.OK;
			CurrentRule.updateForm.DialogResult = CurrentRule.dialog;
			CurrentRule.updateForm.Close();
		}

		public static DialogResult XShowDialog(Form parent, Form subForm)
		{
			bool invokeRequired = parent.InvokeRequired;
			DialogResult result;
			if (invokeRequired)
			{
				CurrentRule.InvokeDelegate method = new CurrentRule.InvokeDelegate(CurrentRule.XShowDialog);
				parent.Invoke(method, new object[]
				{
					parent,
					subForm
				});
				result = subForm.DialogResult;
			}
			else
			{
				DialogResult dialogResult = subForm.ShowDialog(parent);
				result = dialogResult;
			}
			return result;
		}

		private void txt_work_hour_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}

		private void cbx_monday_KeyDown(object sender, KeyEventArgs e)
		{
			e.Handled = true;
		}

		private void rab_recog_mode1_CheckedChanged(object sender, EventArgs e)
		{
			this.datetime_start.Enabled = this.rab_recog_mode1.Checked;
			this.datetime_end.Enabled = this.rab_recog_mode1.Checked;
		}

		private void rab_recog_mode2_CheckedChanged(object sender, EventArgs e)
		{
			this.txt_work_hour.Enabled = this.rab_recog_mode2.Checked;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CurrentRule));
            this.btn_ok = new OpenSoftFace.Setting.Controls.MyButton();
            this.lbl_attend_effect = new System.Windows.Forms.Label();
            this.cbx_sunday = new System.Windows.Forms.CheckBox();
            this.cbx_saturday = new System.Windows.Forms.CheckBox();
            this.cbx_friday = new System.Windows.Forms.CheckBox();
            this.cbx_thursday = new System.Windows.Forms.CheckBox();
            this.cbx_wednesday = new System.Windows.Forms.CheckBox();
            this.cbx_tuesday = new System.Windows.Forms.CheckBox();
            this.cbx_monday = new System.Windows.Forms.CheckBox();
            this.panel_liveness = new System.Windows.Forms.Panel();
            this.datetime_end = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.datetime_start = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.txt_work_hour = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.rab_recog_mode2 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rab_recog_mode1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel_effective_mode = new System.Windows.Forms.Panel();
            this.rab_effective_mode_2 = new System.Windows.Forms.RadioButton();
            this.rab_effective_mode_1 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            this.panel_liveness.SuspendLayout();
            this.panel_effective_mode.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.panel_effective_mode);
            this.splitContainer.Panel2.Controls.Add(this.label6);
            this.splitContainer.Panel2.Controls.Add(this.lbl_attend_effect);
            this.splitContainer.Panel2.Controls.Add(this.cbx_sunday);
            this.splitContainer.Panel2.Controls.Add(this.cbx_saturday);
            this.splitContainer.Panel2.Controls.Add(this.cbx_friday);
            this.splitContainer.Panel2.Controls.Add(this.cbx_thursday);
            this.splitContainer.Panel2.Controls.Add(this.cbx_wednesday);
            this.splitContainer.Panel2.Controls.Add(this.cbx_tuesday);
            this.splitContainer.Panel2.Controls.Add(this.cbx_monday);
            this.splitContainer.Panel2.Controls.Add(this.panel_liveness);
            this.splitContainer.Panel2.Controls.Add(this.label7);
            this.splitContainer.Panel2.Controls.Add(this.label5);
            this.splitContainer.Panel2.Controls.Add(this.btn_ok);
            this.splitContainer.Size = new System.Drawing.Size(430, 338);
            this.splitContainer.SplitterDistance = 52;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(402, 9);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            // 
            // lbl_title
            // 
            this.lbl_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.lbl_title.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl_title.Size = new System.Drawing.Size(430, 52);
            this.lbl_title.Text = "";
            this.lbl_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_ok.EnterForeColor = System.Drawing.Color.White;
            this.btn_ok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_ok.ForeColor = System.Drawing.Color.White;
            this.btn_ok.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.LeaveForeColor = System.Drawing.Color.White;
            this.btn_ok.Location = new System.Drawing.Point(169, 245);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(92, 32);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "关闭";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // lbl_attend_effect
            // 
            this.lbl_attend_effect.AutoSize = true;
            this.lbl_attend_effect.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_attend_effect.Location = new System.Drawing.Point(146, 119);
            this.lbl_attend_effect.Name = "lbl_attend_effect";
            this.lbl_attend_effect.Size = new System.Drawing.Size(117, 17);
            this.lbl_attend_effect.TabIndex = 70;
            this.lbl_attend_effect.Text = "00:00:00 - 23:59:59";
            // 
            // cbx_sunday
            // 
            this.cbx_sunday.AutoCheck = false;
            this.cbx_sunday.AutoSize = true;
            this.cbx_sunday.Checked = true;
            this.cbx_sunday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_sunday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_sunday.Location = new System.Drawing.Point(276, 169);
            this.cbx_sunday.Name = "cbx_sunday";
            this.cbx_sunday.Size = new System.Drawing.Size(51, 21);
            this.cbx_sunday.TabIndex = 66;
            this.cbx_sunday.Text = "周日";
            this.cbx_sunday.UseVisualStyleBackColor = true;
            // 
            // cbx_saturday
            // 
            this.cbx_saturday.AutoCheck = false;
            this.cbx_saturday.AutoSize = true;
            this.cbx_saturday.Checked = true;
            this.cbx_saturday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_saturday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_saturday.Location = new System.Drawing.Point(210, 169);
            this.cbx_saturday.Name = "cbx_saturday";
            this.cbx_saturday.Size = new System.Drawing.Size(51, 21);
            this.cbx_saturday.TabIndex = 65;
            this.cbx_saturday.Text = "周六";
            this.cbx_saturday.UseVisualStyleBackColor = true;
            // 
            // cbx_friday
            // 
            this.cbx_friday.AutoCheck = false;
            this.cbx_friday.AutoSize = true;
            this.cbx_friday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_friday.Location = new System.Drawing.Point(148, 169);
            this.cbx_friday.Name = "cbx_friday";
            this.cbx_friday.Size = new System.Drawing.Size(51, 21);
            this.cbx_friday.TabIndex = 64;
            this.cbx_friday.Text = "周五";
            this.cbx_friday.UseVisualStyleBackColor = true;
            // 
            // cbx_thursday
            // 
            this.cbx_thursday.AutoCheck = false;
            this.cbx_thursday.AutoSize = true;
            this.cbx_thursday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_thursday.Location = new System.Drawing.Point(340, 145);
            this.cbx_thursday.Name = "cbx_thursday";
            this.cbx_thursday.Size = new System.Drawing.Size(51, 21);
            this.cbx_thursday.TabIndex = 63;
            this.cbx_thursday.Text = "周四";
            this.cbx_thursday.UseVisualStyleBackColor = true;
            // 
            // cbx_wednesday
            // 
            this.cbx_wednesday.AutoCheck = false;
            this.cbx_wednesday.AutoSize = true;
            this.cbx_wednesday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_wednesday.Location = new System.Drawing.Point(276, 145);
            this.cbx_wednesday.Name = "cbx_wednesday";
            this.cbx_wednesday.Size = new System.Drawing.Size(51, 21);
            this.cbx_wednesday.TabIndex = 62;
            this.cbx_wednesday.Text = "周三";
            this.cbx_wednesday.UseVisualStyleBackColor = true;
            // 
            // cbx_tuesday
            // 
            this.cbx_tuesday.AutoCheck = false;
            this.cbx_tuesday.AutoSize = true;
            this.cbx_tuesday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_tuesday.Location = new System.Drawing.Point(210, 145);
            this.cbx_tuesday.Name = "cbx_tuesday";
            this.cbx_tuesday.Size = new System.Drawing.Size(51, 21);
            this.cbx_tuesday.TabIndex = 61;
            this.cbx_tuesday.Text = "周二";
            this.cbx_tuesday.UseVisualStyleBackColor = true;
            // 
            // cbx_monday
            // 
            this.cbx_monday.AutoCheck = false;
            this.cbx_monday.AutoSize = true;
            this.cbx_monday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_monday.Location = new System.Drawing.Point(148, 145);
            this.cbx_monday.Name = "cbx_monday";
            this.cbx_monday.Size = new System.Drawing.Size(51, 21);
            this.cbx_monday.TabIndex = 60;
            this.cbx_monday.Text = "周一";
            this.cbx_monday.UseVisualStyleBackColor = true;
            this.cbx_monday.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbx_monday_KeyDown);
            // 
            // panel_liveness
            // 
            this.panel_liveness.Controls.Add(this.datetime_end);
            this.panel_liveness.Controls.Add(this.datetime_start);
            this.panel_liveness.Controls.Add(this.txt_work_hour);
            this.panel_liveness.Controls.Add(this.rab_recog_mode2);
            this.panel_liveness.Controls.Add(this.label3);
            this.panel_liveness.Controls.Add(this.label2);
            this.panel_liveness.Controls.Add(this.rab_recog_mode1);
            this.panel_liveness.Controls.Add(this.label1);
            this.panel_liveness.Controls.Add(this.label4);
            this.panel_liveness.Location = new System.Drawing.Point(30, 19);
            this.panel_liveness.Name = "panel_liveness";
            this.panel_liveness.Size = new System.Drawing.Size(371, 94);
            this.panel_liveness.TabIndex = 69;
            // 
            // datetime_end
            // 
            this.datetime_end.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.datetime_end.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datetime_end.EmptyTextTip = null;
            this.datetime_end.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.datetime_end.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.datetime_end.Location = new System.Drawing.Point(175, 32);
            this.datetime_end.MaxLength = 4;
            this.datetime_end.Name = "datetime_end";
            this.datetime_end.Size = new System.Drawing.Size(53, 23);
            this.datetime_end.TabIndex = 5;
            this.datetime_end.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_work_hour_KeyPress);
            // 
            // datetime_start
            // 
            this.datetime_start.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.datetime_start.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.datetime_start.EmptyTextTip = null;
            this.datetime_start.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.datetime_start.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.datetime_start.Location = new System.Drawing.Point(175, 5);
            this.datetime_start.MaxLength = 4;
            this.datetime_start.Name = "datetime_start";
            this.datetime_start.Size = new System.Drawing.Size(53, 23);
            this.datetime_start.TabIndex = 5;
            this.datetime_start.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_work_hour_KeyPress);
            // 
            // txt_work_hour
            // 
            this.txt_work_hour.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_work_hour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_work_hour.EmptyTextTip = null;
            this.txt_work_hour.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_work_hour.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_work_hour.Location = new System.Drawing.Point(175, 64);
            this.txt_work_hour.MaxLength = 4;
            this.txt_work_hour.Name = "txt_work_hour";
            this.txt_work_hour.Size = new System.Drawing.Size(53, 23);
            this.txt_work_hour.TabIndex = 5;
            this.txt_work_hour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_work_hour_KeyPress);
            // 
            // rab_recog_mode2
            // 
            this.rab_recog_mode2.AutoCheck = false;
            this.rab_recog_mode2.AutoSize = true;
            this.rab_recog_mode2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.rab_recog_mode2.Location = new System.Drawing.Point(3, 64);
            this.rab_recog_mode2.Name = "rab_recog_mode2";
            this.rab_recog_mode2.Size = new System.Drawing.Size(86, 21);
            this.rab_recog_mode2.TabIndex = 4;
            this.rab_recog_mode2.TabStop = true;
            this.rab_recog_mode2.Tag = "2";
            this.rab_recog_mode2.Text = "弹性工作制";
            this.rab_recog_mode2.UseVisualStyleBackColor = true;
            this.rab_recog_mode2.CheckedChanged += new System.EventHandler(this.rab_recog_mode2_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(116, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 52;
            this.label3.Text = "每日工时";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(116, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 52;
            this.label2.Text = "下班时间";
            // 
            // rab_recog_mode1
            // 
            this.rab_recog_mode1.AutoCheck = false;
            this.rab_recog_mode1.AutoSize = true;
            this.rab_recog_mode1.Checked = true;
            this.rab_recog_mode1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rab_recog_mode1.Location = new System.Drawing.Point(4, 4);
            this.rab_recog_mode1.Name = "rab_recog_mode1";
            this.rab_recog_mode1.Size = new System.Drawing.Size(86, 21);
            this.rab_recog_mode1.TabIndex = 1;
            this.rab_recog_mode1.TabStop = true;
            this.rab_recog_mode1.Tag = "1";
            this.rab_recog_mode1.Text = "固定考勤制";
            this.rab_recog_mode1.UseVisualStyleBackColor = true;
            this.rab_recog_mode1.CheckedChanged += new System.EventHandler(this.rab_recog_mode1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(116, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 52;
            this.label1.Text = "上班时间";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(230, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 17);
            this.label4.TabIndex = 52;
            this.label4.Text = "小时";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.Location = new System.Drawing.Point(32, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 67;
            this.label7.Text = "休息日设置";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label5.Location = new System.Drawing.Point(32, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 17);
            this.label5.TabIndex = 68;
            this.label5.Text = "考勤时间范围";
            // 
            // panel_effective_mode
            // 
            this.panel_effective_mode.Controls.Add(this.rab_effective_mode_2);
            this.panel_effective_mode.Controls.Add(this.rab_effective_mode_1);
            this.panel_effective_mode.Location = new System.Drawing.Point(144, 191);
            this.panel_effective_mode.Name = "panel_effective_mode";
            this.panel_effective_mode.Size = new System.Drawing.Size(200, 27);
            this.panel_effective_mode.TabIndex = 72;
            // 
            // rab_effective_mode_2
            // 
            this.rab_effective_mode_2.AutoCheck = false;
            this.rab_effective_mode_2.AutoSize = true;
            this.rab_effective_mode_2.Location = new System.Drawing.Point(95, 4);
            this.rab_effective_mode_2.Name = "rab_effective_mode_2";
            this.rab_effective_mode_2.Size = new System.Drawing.Size(74, 21);
            this.rab_effective_mode_2.TabIndex = 10;
            this.rab_effective_mode_2.TabStop = true;
            this.rab_effective_mode_2.Tag = "2";
            this.rab_effective_mode_2.Text = "立即生效";
            this.rab_effective_mode_2.UseVisualStyleBackColor = true;
            // 
            // rab_effective_mode_1
            // 
            this.rab_effective_mode_1.AutoCheck = false;
            this.rab_effective_mode_1.AutoSize = true;
            this.rab_effective_mode_1.Checked = true;
            this.rab_effective_mode_1.Location = new System.Drawing.Point(4, 4);
            this.rab_effective_mode_1.Name = "rab_effective_mode_1";
            this.rab_effective_mode_1.Size = new System.Drawing.Size(74, 21);
            this.rab_effective_mode_1.TabIndex = 9;
            this.rab_effective_mode_1.TabStop = true;
            this.rab_effective_mode_1.Tag = "1";
            this.rab_effective_mode_1.Text = "次月生效";
            this.rab_effective_mode_1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.Location = new System.Drawing.Point(33, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 17);
            this.label6.TabIndex = 71;
            this.label6.Text = "新规则生效设置";
            // 
            // CurrentRule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(430, 338);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CurrentRule";
            this.ShowInTaskbar = false;
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            this.panel_liveness.ResumeLayout(false);
            this.panel_liveness.PerformLayout();
            this.panel_effective_mode.ResumeLayout(false);
            this.panel_effective_mode.PerformLayout();
            this.ResumeLayout(false);

		}
	}
}
