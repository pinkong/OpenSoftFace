using System;

namespace CommonConstant
{
	public class PersonInfoStatus
	{
		public const int ENABLED = 1;

		public const int DISABLE = 0;

		public const int DELETE = -1;
	}
}
