
using CommonConstant;
using LogUtil;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	public class MyTabControl : TabControl
	{
		private const int CLOSE_SIZE = 20;

		private const int ITEM_WIDTH = 150;

		private const int ITEM_HEIGHT = 35;

		public override Rectangle DisplayRectangle
		{
			get
			{
				Rectangle displayRectangle = base.DisplayRectangle;
				return new Rectangle(displayRectangle.Left - 3, displayRectangle.Top - 1, displayRectangle.Width + 8, displayRectangle.Height + 8);
			}
		}

		public MyTabControl()
		{
			this.SetStyles();
			base.SizeMode = TabSizeMode.Fixed;
			this.Dock = DockStyle.Fill;
			this.Font = new Font("微软雅黑", 10f);
			base.ItemSize = new Size(150, 35);
		}

		private void SetStyles()
		{
			base.SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer, true);
			base.UpdateStyles();
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			this.Refresh();
			base.Update();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			try
			{
				Rectangle clientRectangle = base.ClientRectangle;
				e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
				e.Graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
				using (BufferedGraphics bufferedGraphics = BufferedGraphicsManager.Current.Allocate(e.Graphics, clientRectangle))
				{
					bufferedGraphics.Graphics.FillRectangle(new SolidBrush(ColorUtil.White), clientRectangle);
					bool flag = base.TabPages.Count > 2;
					if (flag)
					{
						int num = base.ClientSize.Width - 70;
						bool flag2 = base.TabCount > 0;
						if (flag2)
						{
							bool flag3 = base.ItemSize.Width * base.TabCount + 30 > num || (num - 30) / base.TabCount > base.ItemSize.Width;
							if (flag3)
							{
								bool flag4 = num < base.TabCount * base.ItemSize.Width + 30 || base.ItemSize.Width < 150;
								if (flag4)
								{
									bool flag5 = base.TabCount > 0;
									if (flag5)
									{
										base.ItemSize = new Size((num - 30) / base.TabCount, 35);
									}
								}
								bool flag6 = base.ItemSize.Width > 150;
								if (flag6)
								{
									base.ItemSize = new Size(150, 35);
								}
							}
						}
						for (int i = 0; i < base.TabCount; i++)
						{
							this.DrawTabPage(bufferedGraphics.Graphics, base.GetTabRect(i), i);
						}
					}
					bufferedGraphics.Render(e.Graphics);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void DrawTabPage(Graphics graphics, Rectangle rectangle, int index)
		{
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
			graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
			StringFormat stringFormat = new StringFormat();
			stringFormat.Trimming = StringTrimming.EllipsisCharacter;
			stringFormat.FormatFlags = StringFormatFlags.NoWrap;
			Rectangle r = new Rectangle(rectangle.X + 20, rectangle.Y + 9, rectangle.Width, base.TabPages[index].Font.Height);
			Point point = new Point(this.GetCloseRect(rectangle).X, 13);
			try
			{
				bool flag = index != base.TabCount - 1;
				if (flag)
				{
					bool flag2 = index == base.SelectedIndex;
					if (flag2)
					{
						graphics.FillPath(new SolidBrush(ColorUtil.LightBlack), this.CreateTabPath(rectangle));
						graphics.DrawString(base.TabPages[index].Text, base.TabPages[index].Font, new SolidBrush(ColorUtil.White), r, stringFormat);
						bool flag3 = index != 0;
						if (flag3)
						{
							/*using (Bitmap close = Resources.Close)
							{
								graphics.DrawImage(close, point);
							}*/
						}
					}
					else
					{
						graphics.DrawPath(new Pen(ColorUtil.Black), this.CreateRightTabPath(rectangle));
						graphics.DrawString(base.TabPages[index].Text, base.TabPages[index].Font, new SolidBrush(ColorUtil.Black), r, stringFormat);
						bool flag4 = index != 0;
						if (flag4)
						{
							/*using (Bitmap close__4_ = Resources.close__4_)
							{
								graphics.DrawImage(close__4_, point);
							}*/
						}
					}
				}
			}
			catch (NullReferenceException ex)
			{
				LogHelper.LogError(base.GetType(), ex);
				Console.WriteLine(ex);
			}
		}

		protected override void OnSelecting(TabControlCancelEventArgs e)
		{
			bool flag = e.TabPageIndex == base.TabPages.Count - 1;
			if (flag)
			{
				e.Cancel = true;
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);
			bool flag = e.Button == MouseButtons.Left;
			if (flag)
			{
				bool flag2 = base.SelectedIndex == 0;
				if (!flag2)
				{
					int x = e.X;
					int y = e.Y;
					Rectangle tabRect = base.GetTabRect(base.SelectedIndex);
					tabRect.Offset(tabRect.Width - 28, 5);
					tabRect.Width = 20;
					tabRect.Height = 20;
					bool flag3 = x > tabRect.X && x < tabRect.Right - 5 && y > tabRect.Y && y < tabRect.Bottom;
					bool flag4 = flag3;
					if (flag4)
					{
						bool flag5 = base.TabPages.Count > 2;
						if (flag5)
						{
							try
							{
								TabPage selectedTab = base.SelectedTab;
								base.TabPages.Remove(selectedTab);
								base.SelectedTab.Refresh();
								base.SelectedIndex = base.TabPages.Count - 2;
								selectedTab.Dispose();
							}
							catch (Exception ex)
							{
								LogHelper.LogError(base.GetType(), ex);
								Console.WriteLine(ex);
							}
							finally
							{
								GC.Collect();
								GC.WaitForPendingFinalizers();
							}
						}
						else
						{
							Environment.Exit(0);
							base.Dispose();
						}
					}
				}
			}
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			int x = e.X;
			int y = e.Y;
			bool flag = false;
			for (int i = 1; i < base.TabPages.Count; i++)
			{
				Rectangle tabRect = base.GetTabRect(i);
				tabRect.Offset(tabRect.Width - 28, 5);
				tabRect.Width = 20;
				tabRect.Height = 20;
				flag = (x > tabRect.X && x < tabRect.Right - 5 && y > tabRect.Y && y < tabRect.Bottom);
				bool flag2 = flag;
				if (flag2)
				{
					break;
				}
			}
			bool flag3 = flag;
			if (flag3)
			{
				this.Cursor = Cursors.Hand;
			}
			else
			{
				this.Cursor = Cursors.Default;
			}
		}

		private GraphicsPath CreateTabPath(Rectangle rect)
		{
			GraphicsPath graphicsPath = new GraphicsPath();
			graphicsPath.AddLine(rect.Left - 1, rect.Top, rect.Left - 1, rect.Top);
			graphicsPath.AddLine(rect.Left - 1, rect.Top, rect.Right, rect.Top);
			graphicsPath.AddLine(rect.Right, rect.Top, rect.Right, rect.Bottom);
			graphicsPath.AddLine(rect.Right, rect.Bottom + 1, rect.Left - 1, rect.Bottom + 1);
			graphicsPath.CloseFigure();
			return graphicsPath;
		}

		private GraphicsPath CreateRightTabPath(Rectangle rect)
		{
			GraphicsPath graphicsPath = new GraphicsPath();
			graphicsPath.AddLine(rect.Right, rect.Top + 5, rect.Right, rect.Bottom - 5);
			return graphicsPath;
		}

		private Rectangle GetCloseRect(Rectangle myTabRect)
		{
			myTabRect.Offset(myTabRect.Width - 28, 5);
			myTabRect.Width = 20;
			myTabRect.Height = 20;
			return myTabRect;
		}
	}
}
