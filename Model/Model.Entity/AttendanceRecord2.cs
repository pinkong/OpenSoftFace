using System;

namespace Model.Entity
{
	public class AttendanceRecord2 : AttendanceRecord
	{
		public string PersonName
		{
			get;
			set;
		}

		public string WeekText
		{
			get;
			set;
		}

		public string AttendStatusText
		{
			get;
			set;
		}
	}
}
