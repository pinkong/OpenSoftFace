using OpenSoftFace.Setting.Controls;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Forms
{
	public class WarningDialogForm : DragBaseForm
	{
		public delegate DialogResult InvokeDelegate(Form parent, Form subForm);

		private static WarningDialogForm warningForm;

		private static DialogResult dialog;

		public static int labelWidth = 10;

		private IContainer components = null;

		private MyButton btn_ok;

		private MyButton btn_cancel;

		private Label lbl_warningMsg;

		private PictureBox pic_warningMsg;

		public WarningDialogForm()
		{
			this.InitializeComponent();
		}

		public static DialogResult showWarningMsg(string msg, string title = null, bool isChangge = false, Form parent = null)
		{
			WarningDialogForm.warningForm = new WarningDialogForm();
			WarningDialogForm.warningForm.lbl_warningMsg.Text = msg;
			WarningDialogForm.warningForm.TopMost = true;
			bool flag = !string.IsNullOrEmpty(msg);
			if (flag)
			{
				try
				{
					int width = TextRenderer.MeasureText(msg, WarningDialogForm.warningForm.lbl_warningMsg.Font).Width;
					int num = StringUtil.SearchCharCount(msg, "\r");
					int num2 = width / 180 + num + 1;
					int num3 = num2 * 25;
					WarningDialogForm.warningForm.Height += num3;
					WarningDialogForm.warningForm.lbl_warningMsg.AutoSize = false;
					WarningDialogForm.warningForm.splitContainer.Size = new Size(WarningDialogForm.warningForm.splitContainer.Width, WarningDialogForm.warningForm.splitContainer.Height + num3);
					WarningDialogForm.warningForm.splitContainer.Panel2.Size = new Size(WarningDialogForm.warningForm.splitContainer.Panel2.Width, WarningDialogForm.warningForm.splitContainer.Panel2.Height + num3);
					WarningDialogForm.warningForm.btn_ok.Location = new Point(WarningDialogForm.warningForm.btn_ok.Location.X, WarningDialogForm.warningForm.btn_ok.Location.Y + num3);
					WarningDialogForm.warningForm.btn_cancel.Location = new Point(WarningDialogForm.warningForm.btn_cancel.Location.X, WarningDialogForm.warningForm.btn_cancel.Location.Y + num3);
					WarningDialogForm.warningForm.lbl_warningMsg.Size = new Size(200, WarningDialogForm.warningForm.lbl_warningMsg.Height + num3);
					WarningDialogForm.warningForm.lbl_warningMsg.Text = msg;
				}
				catch (Exception se)
				{
					LogHelper.LogError(typeof(WarningDialogForm), se);
				}
			}
			bool flag2 = !string.IsNullOrEmpty(title);
			if (flag2)
			{
				WarningDialogForm.warningForm.lbl_title.Text = title;
			}
			bool flag3 = parent != null;
			DialogResult result;
			if (flag3)
			{
				result = WarningDialogForm.XShowDialog(parent, WarningDialogForm.warningForm);
			}
			else
			{
				WarningDialogForm.warningForm.ShowDialog();
				result = WarningDialogForm.dialog;
			}
			return result;
		}

		private void ok_Click(object sender, EventArgs e)
		{
			WarningDialogForm.dialog = DialogResult.OK;
			WarningDialogForm.warningForm.DialogResult = WarningDialogForm.dialog;
			WarningDialogForm.warningForm.Close();
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			WarningDialogForm.dialog = DialogResult.Cancel;
			WarningDialogForm.warningForm.DialogResult = WarningDialogForm.dialog;
			WarningDialogForm.warningForm.Close();
		}

		public override void Close_btn_Click(object sender, EventArgs e)
		{
			WarningDialogForm.dialog = DialogResult.Cancel;
			WarningDialogForm.warningForm.DialogResult = WarningDialogForm.dialog;
			WarningDialogForm.warningForm.Close();
		}

		public static DialogResult XShowDialog(Form parent, Form subForm)
		{
			bool invokeRequired = parent.InvokeRequired;
			DialogResult result;
			if (invokeRequired)
			{
				WarningDialogForm.InvokeDelegate method = new WarningDialogForm.InvokeDelegate(WarningDialogForm.XShowDialog);
				parent.Invoke(method, new object[]
				{
					parent,
					subForm
				});
				result = subForm.DialogResult;
			}
			else
			{
				DialogResult dialogResult = subForm.ShowDialog(parent);
				result = dialogResult;
			}
			return result;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WarningDialogForm));
            this.btn_ok = new OpenSoftFace.Setting.Controls.MyButton();
            this.btn_cancel = new OpenSoftFace.Setting.Controls.MyButton();
            this.lbl_warningMsg = new System.Windows.Forms.Label();
            this.pic_warningMsg = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_warningMsg)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.pic_warningMsg);
            this.splitContainer.Panel2.Controls.Add(this.btn_ok);
            this.splitContainer.Panel2.Controls.Add(this.lbl_warningMsg);
            this.splitContainer.Panel2.Controls.Add(this.btn_cancel);
            this.splitContainer.Size = new System.Drawing.Size(375, 188);
            this.splitContainer.SplitterDistance = 52;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(350, 9);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            // 
            // lbl_title
            // 
            this.lbl_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.lbl_title.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl_title.Size = new System.Drawing.Size(375, 52);
            this.lbl_title.Text = "系统提示";
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_ok.EnterForeColor = System.Drawing.Color.White;
            this.btn_ok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_ok.ForeColor = System.Drawing.Color.White;
            this.btn_ok.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.LeaveForeColor = System.Drawing.Color.White;
            this.btn_ok.Location = new System.Drawing.Point(85, 88);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(92, 32);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "确认";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.White;
            this.btn_cancel.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_cancel.EnterForeColor = System.Drawing.Color.Black;
            this.btn_cancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_cancel.ForeColor = System.Drawing.Color.Black;
            this.btn_cancel.LeaveBackColor = System.Drawing.Color.White;
            this.btn_cancel.LeaveForeColor = System.Drawing.Color.Black;
            this.btn_cancel.Location = new System.Drawing.Point(198, 88);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(92, 32);
            this.btn_cancel.TabIndex = 1;
            this.btn_cancel.Text = "取消";
            this.btn_cancel.UseVisualStyleBackColor = false;
            this.btn_cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // lbl_warningMsg
            // 
            this.lbl_warningMsg.AutoEllipsis = true;
            this.lbl_warningMsg.AutoSize = true;
            this.lbl_warningMsg.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lbl_warningMsg.Location = new System.Drawing.Point(114, 31);
            this.lbl_warningMsg.Name = "lbl_warningMsg";
            this.lbl_warningMsg.Size = new System.Drawing.Size(127, 24);
            this.lbl_warningMsg.TabIndex = 2;
            this.lbl_warningMsg.Text = "确定要删除吗?";
            // 
            // pic_warningMsg
            // 
            this.pic_warningMsg.Location = new System.Drawing.Point(69, 28);
            this.pic_warningMsg.Name = "pic_warningMsg";
            this.pic_warningMsg.Size = new System.Drawing.Size(30, 30);
            this.pic_warningMsg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_warningMsg.TabIndex = 3;
            this.pic_warningMsg.TabStop = false;
            // 
            // WarningDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(375, 188);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "WarningDialogForm";
            this.ShowInTaskbar = false;
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_warningMsg)).EndInit();
            this.ResumeLayout(false);

		}
	}
}
