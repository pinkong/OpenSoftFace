using DAL.Service;
using DAL.Service.Impl;
using LogUtil;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Common
{
	public class OperatorUtil
	{
		public static void DeleteTempFile(string rootPath)
		{
			try
			{
				string path = rootPath + "\\temp\\";
				bool flag = Directory.Exists(path);
				if (flag)
				{
					Directory.Delete(path, true);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(OperatorUtil), se);
			}
		}

		public static void DeleteInvalidShowFile(string rootPath)
		{
			try
			{
				IMainInfoService mainInfoService = new MainInfoServiceImpl();
				List<MainInfo> list = mainInfoService.QueryForList(new string[]
				{
					"p_valid != -1"
				}, null, null, null, -1, -1);
				List<string> list2 = new List<string>();
				bool flag = list != null && list.Count > 0;
				if (flag)
				{
					foreach (MainInfo current in list)
					{
						bool flag2 = current != null && !string.IsNullOrWhiteSpace(current.ShowImageName);
						if (flag2)
						{
							bool flag3 = !list2.Contains(current.ShowImageName);
							if (flag3)
							{
								list2.Add(current.ShowImageName);
							}
						}
					}
				}
				string path = rootPath + "\\show\\";
				bool flag4 = Directory.Exists(path);
				if (flag4)
				{
					string[] files = Directory.GetFiles(path);
					bool flag5 = files != null && files.Length != 0;
					if (flag5)
					{
						string[] array = files;
						for (int i = 0; i < array.Length; i++)
						{
							string text = array[i];
							string item = text.Substring(text.LastIndexOf("\\") + 1);
							bool flag6 = !list2.Contains(item);
							if (flag6)
							{
								try
								{
									File.Delete(text);
								}
								catch
								{
								}
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(OperatorUtil), se);
			}
		}

		public static void DeleteInvalidRegisFile(string rootPath)
		{
			try
			{
				IFeatureInfoService featureInfoService = new FeatureInfoServiceImpl();
				List<FeatureInfo> list = featureInfoService.QueryForList(new string[]
				{
					"f_valid != -1"
				}, null, null, null, -1, -1);
				List<string> list2 = new List<string>();
				bool flag = list != null && list.Count > 0;
				if (flag)
				{
					foreach (FeatureInfo current in list)
					{
						bool flag2 = current != null && !string.IsNullOrWhiteSpace(current.RegisImageName);
						if (flag2)
						{
							bool flag3 = !list2.Contains(current.RegisImageName);
							if (flag3)
							{
								list2.Add(current.RegisImageName);
							}
						}
					}
				}
				string path = rootPath + "\\source\\";
				bool flag4 = Directory.Exists(path);
				if (flag4)
				{
					string[] files = Directory.GetFiles(path);
					bool flag5 = files != null && files.Length != 0;
					if (flag5)
					{
						string[] array = files;
						for (int i = 0; i < array.Length; i++)
						{
							string text = array[i];
							string item = text.Substring(text.LastIndexOf("\\") + 1);
							bool flag6 = !list2.Contains(item);
							if (flag6)
							{
								try
								{
									File.Delete(text);
								}
								catch
								{
								}
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(OperatorUtil), se);
			}
		}

		public static void RestartExe()
		{
			try
			{
				Process[] processesByName = Process.GetProcessesByName("ArcOffice");
				bool flag = processesByName.Length != 0;
				if (flag)
				{
					for (int i = 0; i < processesByName.Length; i++)
					{
						processesByName[i].Kill();
					}
				}
				try
				{
					Thread.Sleep(2000);
					Process process = new Process();
					ProcessStartInfo processStartInfo = new ProcessStartInfo();
					processStartInfo.FileName = "ArcOffice.exe";
					processStartInfo.WindowStyle = ProcessWindowStyle.Normal;
					process.StartInfo = processStartInfo;
					processStartInfo.Verb = "runas";
					process.StartInfo.UseShellExecute = false;
					process.Start();
				}
				catch
				{
				}
				Application.Exit();
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(OperatorUtil), se);
			}
		}
	}
}
