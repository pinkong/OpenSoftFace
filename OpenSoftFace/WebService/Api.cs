﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using Model.Entity;
using DAL.Service;
using DAL.Service.Impl;
using Utils;
using OpenSoftFace.Setting.Common;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using LitJson2;
using System.Web;

namespace OpenSoftFace.WebService
{
    public class Api : IHttpHandler
    {
        private StringBuilder sqlBuilder = new StringBuilder();
        private IMainInfoService personInfoService;
        private IAttendanceRecordService attendanceRecordService;
        private DateTime search_start;
        private DateTime search_end;
        private DateTime tempDateTime = DateTime.Now;
        public static DataTable dt = new DataTable();
        public static String workStart = "9:00";
        public static String workRestStart = "12:30";
        public static String workRestEnd = "13:59";
        public static String workEnd = "18:00";
        public static String staffFisrtTime = "";
        public static String staffRestStart = "";
        public static String staffRestEnd = "";
        public static String staffLastTime = "";
        public void ProcessRequest(HttpContext context)
        {
            var _array = ParseQueryString("http://"+WebHttpService.severIPAddress+context.Request.Url);
            if (_array.ContainsKey("start")) { 
                search_start=Convert.ToDateTime(_array["start"]);
            }
            if (_array.ContainsKey("end"))
            {
                search_end = Convert.ToDateTime(_array["end"]);
            }
            if (_array.ContainsKey("month") && _array.ContainsKey("year"))
            {
                search_start = DateTimeUtil.getMonthFirstDay(Convert.ToDateTime(_array["year"] + "-" + _array["month"] + "-1"));
                search_end = DateTimeUtil.getMonthLastDay(Convert.ToDateTime(_array["year"] + "-" + _array["month"] + "-1"));
            }

            StringBuilder sbText = new StringBuilder();
            if (context.Request.Url.Contains("action=excel"))
            {
               String file= excel();
               context.Response.Body = File.ReadAllBytes(file);
               context.Response.StateCode = "200";
               context.Response.ContentType = "multipart/form-data";
               context.Response.StateDescription = "OK";
               context.Response.ContentDisposition = "attachment;fileName=" + file;
               return;
            }

            else if (context.Request.Url.Contains("action=html"))
            {

                sbText.Append("<html>");
                sbText.Append("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/><title>员工考勤</title></head>");
                //sbText.Append("<style type='text/css'>th,td{border:1px solid black;padding:2px;}table{padding:20px;}</style></head>");
                sbText.Append("<body style='text-align:center;'>");
                //sbText.Append("<h1>银旅通考勤</h1>");
                sbText.Append(getHtmlNew(0));
                //sbText.Append("<table align='center' cellpadding='1' cellspacing='1'><thead><tr><td>ID</td><td>用户名</td></tr></thead>");
                //sbText.Append("<tbody>xxxxxxx");
                //sbText.Append(GetDataList());
                //sbText.Append("</tbody></table>");
                //sbText.Append("<h1>API list:</h1>");
                //sbText.Append("<h3>1.API Monitor:</h3>");
                //sbText.Append("<h3>2.API Total:</h3>");
                //sbText.Append(string.Format("<h3>更新时间：{0}</h3>", DateTime.Now.ToString()));
                sbText.Append("<div>导出方式</div>");
                sbText.Append("<div>1、直接复制粘贴到excel</div>");
                sbText.Append("<div>2、使用游览器打印pdf功能，pdf转Excel:http://www.pdfdo.com/pdf-to-excel.aspx</div>");
                sbText.Append("</body>");
                sbText.Append("</html>");
            }
            else if (context.Request.Url.Contains("action=htmlnew"))
            {

                sbText.Append("<html>");
                sbText.Append("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/><title>员工考勤</title></head>");
                //sbText.Append("<style type='text/css'>th,td{border:1px solid black;padding:2px;}table{padding:20px;}</style></head>");
                sbText.Append("<body style='text-align:center;'>");
                //sbText.Append("<h1>银旅通考勤</h1>");
                sbText.Append(getHtmlNew(0));
                //sbText.Append("<table align='center' cellpadding='1' cellspacing='1'><thead><tr><td>ID</td><td>用户名</td></tr></thead>");
                //sbText.Append("<tbody>xxxxxxx");
                //sbText.Append(GetDataList());
                //sbText.Append("</tbody></table>");
                //sbText.Append("<h1>API list:</h1>");
                //sbText.Append("<h3>1.API Monitor:</h3>");
                //sbText.Append("<h3>2.API Total:</h3>");
                //sbText.Append(string.Format("<h3>更新时间：{0}</h3>", DateTime.Now.ToString()));
                sbText.Append("<div>pdf转Excel:http://www.pdfdo.com/pdf-to-excel.aspx</div>");
                sbText.Append("</body>");
                sbText.Append("</html>");
            }
            //sbText.Append("Api test!");
            context.Response.Body = Encoding.UTF8.GetBytes(sbText.ToString());
            context.Response.StateCode = "200";
            context.Response.ContentType = "text/html";
            context.Response.StateDescription = "OK";
        }

        public static String getStaff(int monthPaint = 0)
        {
            if (DateTime.Now.Day <= 10)
            {
                monthPaint = 1;
            }
            String staffNames = "";
            //string strFilePath = System.Windows.Forms.Application.StartupPath + "/log/log_faceDetect.txt";
            //FileStream fs = new FileStream(strFilePath, FileMode.Open, FileAccess.Read);
            //StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
            try
            {
                string strFilePath = System.Windows.Forms.Application.StartupPath + "/log/log_faceDetect.txt";
                string[] textArr = File.ReadAllLines(strFilePath, System.Text.Encoding.Default);
                string strLine = "";
                for (int ip = 0; ip < textArr.Length; ip++)
                {
                    strLine = textArr[ip];
                    //string strLine = sr.ReadLine();
                    //while (strLine != null)
                    //{
                    string[] strArray = new string[4];
                    strArray = strLine.Split('_');
                    DateTime d;
                    if (DateTime.Now.Month != 1)
                    {
                        d = new DateTime(DateTime.Now.Year, DateTime.Now.Month - monthPaint, 1);
                    }
                    else
                    {
                        d = new DateTime(DateTime.Now.Year - 1, 12, 1);
                    }

                    if (!strArray[0].Contains(d.ToString("yyyy-MM")))
                    {
                        continue;
                    }
                    if (staffNames.Contains(strArray[1].Split('-')[0]))
                    {
                        //strLine = sr.ReadLine();
                        continue;
                    }
                    if (staffNames == "")
                    {
                        staffNames = strArray[1].Split('-')[0];
                    }
                    else
                    {
                        staffNames = staffNames + "-" + strArray[1].Split('-')[0];
                    }
                    //strLine = sr.ReadLine();
                    //}
                    //sr.Close();
                    //fs.Close();
                }
            }
            catch (Exception err)
            {
                //staffNames = "霍淦碔-杨晓霞-严总-林宏贤-杨文辉-庄总-刘兴炎-张育珠-周熙敏-赵卫根-薛志伟-郑伟-黄炳桂-黄总-刘国辉-区丽敏-李玉海-郭大涛-张郁琪-李芳-蔡施宏-晓勇-许富春-罗燕-邱国停";
                return "";
                //sr.Close();
                //fs.Close();
            }
            return staffNames;
        }

        public String getHtmlNew(int monthPaint = 0)
        {
            string tempStr = "";
            this.personInfoService = new MainInfoServiceImpl();
            this.attendanceRecordService = new AttendanceRecordServiceImpl();
            string[] where = string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				});
            List<MainInfo> tempExportList = this.personInfoService.QueryForList(where, null, null, " id desc ", -1, -1);
            bool flag = tempExportList == null || tempExportList.Count <= 0;
            if (flag)
            {
                return "";
            }
            else
            {
                bool flag2 = tempExportList != null && tempExportList.Count > 0;
                if (flag2)
                {
                    string dir = "./";
                    Dictionary<string, List<DateTime>> dateTimeDict = new Dictionary<string, List<DateTime>>();
                    Dictionary<string, List<string>> columnsDict = DateTimeUtil.getDayDict(this.search_start, this.search_end, ref dateTimeDict, "MM-dd");
                    string arg = string.Format(this.search_start.ToString("yyyy年MM") + "月考勤记录{0}.xls", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                    List<string> list = new List<string>();
                    list.Insert(0, "工号");
                    list.Insert(0, "姓名");
                    list.Insert(0, "");
                    Dictionary<int, string> dictionary = new Dictionary<int, string>();
                    Dictionary<string, List<MainInfo>> dictionary2 = new Dictionary<string, List<MainInfo>>();
                    bool flag4 = dateTimeDict == null || dateTimeDict.Count <= 0;
                    if (flag4)
                    {
                        // this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                    }
                    foreach (string current in columnsDict.Keys)
                    {
                        List<MainInfo> list2 = new List<MainInfo>();
                        list2.AddRange(tempExportList);
                        List<DateTime> list3 = dateTimeDict[current];
                        bool flag5 = list3 != null && list3.Count > 0;
                        if (flag5)
                        {
                            dictionary2.Add(current, this.getExportAttendRecord2List(list2, list3[0], list3[list3.Count - 1]));
                        }
                    }

                    IniHelper iniHelper = new IniHelper("setting.ini", "setting");
                    string company_name = HttpUtility.UrlDecode(iniHelper.Dict["COMPANY_NAME"]);
                    int work_delay_min=int.Parse(iniHelper.Get("WORK_DELAY_MIN"));
                    if (company_name == null) company_name = "";
                    IAttendRuleService attendRuleService = new AttendRuleServiceImpl();
                    AttendRuleParam attendRuleParam = new AttendRuleParam();
                    AttendRule attendRule=attendRuleService.Get(true);
                    attendRuleParam = JsonMapper.ToObject<AttendRuleParam>(attendRule.Json);

                    tempStr += "<link rel='stylesheet' type='text/css' href='https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.css' />";
                    tempStr += "<style type='text/css'>body{margin:20px auto;max-width:1000px;}td{text-align: center;}th,td{border:1px solid black!important;padding:0px!important;}.center th{text-align: center!important;}table{padding:20px!important;margin-bottom: 0px!important;}</style>";
                    int ipage = 1;
                    tempStr += "<h3>考勤表</h3>";

                    foreach (string current in columnsDict.Keys)
                    {
                        List<MainInfo> list2 = dictionary2[current];
                        for (int i = 0; i < list2.Count; i++)
                        {
                            int workDayNum = this.search_end.Day;//工作天数
                            int carkDayNum = 0;//出勤天数
                            int lasyDayNum = 0;//迟到次数
                            int noworkDayNum = 0;//缺勤天数
                            int fastDayNum = 0;//早退天数
                            if (ipage % 2 == 1)
                            {
                                tempStr += "<div style='page-break-after: always;'>";
                            }
                            tempStr += "<table class='table table-bordered'>";
                            tempStr += "<tr><th colspan='2'>单位：" + company_name + "</th><th colspan='2'>姓名：" + list2[i].PersonName + "</th><th colspan='2'>工号：" + list2[i].PersonSerial + "</th><th colspan='2'>部门：" + list2[i].Dept + "</th>" + "<th colspan='4'>日期：" + this.search_start.ToString("yyyy-MM-dd") + "至" + this.search_end.ToString("yyyy-MM-dd") + "</th></tr>";
                            //sw.WriteLine(tthempStr);
                            tempStr += "<tr><th colspan='2'>工作天数：<div style='color: #ddd;display: inline-block;'>{workDayNum}</div></th><th  colspan='2'>出勤天数：<div style='color: #ddd;display: inline-block;'>{carkDayNum}</div></th><th colspan='2'>迟到次数：<div style='color: #ddd;display: inline-block;'>{lasyDayNum}</div></th><th  colspan='2'>早退次数：<div style='color: #ddd;display: inline-block;'>{fastDayNum}</div></th><th  colspan='2'>缺勤天数：<div style='color: #ddd;display: inline-block;'>{noworkDayNum}</div></th><th colspan='2'>加班天数：</th></tr>";
                            tempStr += "<!--tr class='center'><th colspan='2'>工作</th><th colspan='2'>上午</th><th colspan='2'>下午</th><th colspan='2'>工作</th><th colspan='2'>上午</th><th colspan='2'>下午</th></tr-->";
                            tempStr += "<tr class='center'><th>日期</th><th>星期</th><th colspan='2'>上班</th><th colspan='2'>下班</th><th>日期</th><th>星期</th><th colspan='2'>上班</th><th colspan='2'>下班</th></tr>";

                            List<AttendanceRecord2> attendRecordList = list2[i].attendRecordList;
                            bool flagx = attendRecordList != null && attendRecordList.Count > 0;
                            if (flagx)
                            {
                                for (int j = 0; j <= 15; j++)
                                {
                                    if (attendRecordList.Count - 1 < j) {
                                        break;
                                    }
                                    attendRecordList[j] = attendRecordList[j];
                                    if (attendRecordList[j].StartTime == null) {
                                        attendRecordList[j].StartTime = "";
                                    }
                                    if (attendRecordList[j].EndTime == null)
                                    {
                                        attendRecordList[j].EndTime = "";
                                    }
                                    if ((attendRecordList[j].WeekText.ToString() == "六" || attendRecordList[j].WeekText == "日") &&
                                        (attendRecordList[j].StartTime == "" && attendRecordList[j].EndTime == ""))
                                    {
                                        attendRecordList[j].StartTime = "-";
                                        attendRecordList[j].EndTime = "-";
                                        workDayNum = workDayNum - 1;
                                    }

                                    if(attendRecordList[j].StartTime.Length>=2){
                                        DateTime d = DateTime.ParseExact(attendRecordList[j].Date+" "+attendRecordList[j].StartTime, "yyyy/MM/dd hh:mm:ss", System.Globalization.CultureInfo.CurrentCulture);
                                        work_delay_min=-work_delay_min;
                                        DateTime newdt=d.AddMinutes(work_delay_min);
                                        attendRecordList[j].StartTime = newdt.ToString("hh:mm");
                                    }


                                    if (attendRecordList[j].StartTime.Length>=2 && int.Parse(attendRecordList[j].StartTime.Substring(0, 2)) >= int.Parse(attendRuleParam.attend_start_time.Substring(0, 2)))
                                    {
                                        attendRecordList[j].StartTime = attendRecordList[j].StartTime.Substring(0, 5) + "*";
                                        lasyDayNum = lasyDayNum + 1;
                                    }
                                    else if (attendRecordList[j].StartTime.Length >= 2)
                                    {
                                        attendRecordList[j].StartTime = attendRecordList[j].StartTime.Substring(0, 5);
                                    }

                                    if (attendRecordList[j].EndTime.Length >= 2 && int.Parse(attendRecordList[j].EndTime.Substring(0, 2)) < int.Parse(attendRuleParam.attend_end_time.Substring(0, 2)))
                                    {
                                        attendRecordList[j].EndTime = attendRecordList[j].EndTime.Substring(0, 5) + "*";
                                        fastDayNum = fastDayNum + 1;
                                    }
                                    else if (attendRecordList[j].EndTime.Length >= 2)
                                    {
                                        attendRecordList[j].EndTime = attendRecordList[j].EndTime.Substring(0, 5);
                                    }

                                    if (attendRecordList[j].StartTime.Length >= 2 || attendRecordList[j].EndTime.Length >= 2)
                                    {
                                        carkDayNum = carkDayNum + 1;
                                    }
                            

                                    if (attendRecordList[j].StartTime.Length == 0 && attendRecordList[j].EndTime.Length == 0)
                                    {
                                        noworkDayNum = noworkDayNum + 1;
                                    }
                                    tempStr += "<tr><td>" + attendRecordList[j].Date.Substring(5) + "</td><td>" + attendRecordList[j].WeekText + "</td><td colspan='2'>" + attendRecordList[j].StartTime + "</td><td colspan='2'>" + attendRecordList[j].EndTime + "</td>";
                                    if (j + 15 <= attendRecordList.Count - 1)
                                    {
                                        if (attendRecordList[j+15].StartTime == null)
                                        {
                                            attendRecordList[j + 15].StartTime = "";
                                        }
                                        if (attendRecordList[j + 15].EndTime == null)
                                        {
                                            attendRecordList[j + 15].EndTime = "";
                                        }
                                        if ((attendRecordList[j + 15].WeekText.ToString() == "六" || attendRecordList[j + 15].WeekText == "日") &&
                                            (attendRecordList[j + 15].StartTime == "" && attendRecordList[j + 15].EndTime == ""))
                                        {
                                            attendRecordList[j + 15].StartTime = "-";
                                            attendRecordList[j + 15].EndTime = "-";
                                            workDayNum = workDayNum - 1;
                                        }
                                        if (attendRecordList[j + 15].StartTime.Length >= 2 && int.Parse(attendRecordList[j + 15].StartTime.Substring(0, 2)) >= int.Parse(attendRuleParam.attend_start_time.Substring(0, 2)))
                                        {
                                            attendRecordList[j + 15].StartTime = attendRecordList[j + 15].StartTime.Substring(0, 5) + "*";
                                            lasyDayNum = lasyDayNum + 1;
                                        }
                                        else if (attendRecordList[j+15].StartTime.Length >= 2)
                                        {
                                            attendRecordList[j + 15].StartTime = attendRecordList[j + 15].StartTime.Substring(0, 5);
                                        }

                                        if (attendRecordList[j + 15].EndTime.Length >= 2 && int.Parse(attendRecordList[j + 15].EndTime.Substring(0, 2)) < int.Parse(attendRuleParam.attend_end_time.Substring(0, 2)))
                                        {
                                            attendRecordList[j + 15].EndTime = attendRecordList[j + 15].EndTime.Substring(0, 5) + "*";
                                            fastDayNum = fastDayNum + 1;
                                        }
                                        else if (attendRecordList[j+15].EndTime.Length >= 2)
                                        {
                                            attendRecordList[j + 15].EndTime = attendRecordList[j + 15].EndTime.Substring(0, 5);
                                        }

                                        if (attendRecordList[j+15].StartTime.Length >= 2 || attendRecordList[j+15].EndTime.Length >= 2)
                                        {
                                            carkDayNum = carkDayNum + 1;
                                        }
                                        if (attendRecordList[j+15].StartTime.Length == 0 && attendRecordList[j+15].EndTime.Length == 0)
                                        {
                                            noworkDayNum = noworkDayNum + 1;
                                        }
                                        tempStr += "<td>" + attendRecordList[j + 15].Date.Substring(5) + "</td><td>" + attendRecordList[j + 15].WeekText + "</td><td colspan='2'>" + attendRecordList[j + 15].StartTime + "</td><td colspan='2'>" + attendRecordList[j + 15].EndTime + "</td></tr>";
                                    }
                                    else {
                                        tempStr += "<td></td><td></td><td colspan='2'></td><td colspan='2'></td></tr>";
                                    }
                                }
                            }
                            tempStr += "</table><div style='float:right;margin-right:200px;margin-bottom: 40px;'>员工签字：</div><br>";
                            if (ipage % 2 == 0)
                            {
                                tempStr += "</div>";
                            }
                            if (ipage == list2.Count)
                            {
                                tempStr += "</div>";
                            }
                            ipage++;
                            tempStr = tempStr.Replace("{workDayNum}", workDayNum.ToString());
                            tempStr = tempStr.Replace("{noworkDayNum}", noworkDayNum.ToString());
                            tempStr = tempStr.Replace("{carkDayNum}", carkDayNum.ToString());
                            tempStr = tempStr.Replace("{lasyDayNum}", lasyDayNum.ToString());
                            tempStr = tempStr.Replace("{fastDayNum}", fastDayNum.ToString());
                        }
                    }
                }
                else
                {
                    return tempStr;
                }
            }

            return tempStr;
        }

        public static String getHtml(int monthPaint = 0)
        {
            if (DateTime.Now.Day <= 10)
            {
                monthPaint = 1;
            }
            String staffNames = getStaff();
            string[] staffNamesList = staffNames.Split('-');
            string tempStr = "";
            tempStr += "<link rel='stylesheet' type='text/css' href='https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.css' />";
            tempStr += "<style type='text/css'>body{margin:20px auto;max-width:1000px;}td{text-align: center;}th,td{border:1px solid black!important;padding:0px!important;}.center th{text-align: center!important;}table{padding:20px!important;margin-bottom: 0px!important;}</style>";
            int ipage = 1;
            tempStr += "<h3>银旅通考勤</h3>";
            foreach (string item in staffNamesList)
            {
                DateTime d;
                if (DateTime.Now.Month != 1)
                {
                    d = new DateTime(DateTime.Now.Year, DateTime.Now.Month - monthPaint, 1);
                }
                else
                {
                    d = new DateTime(DateTime.Now.Year - 1, 12, 1);
                }
                d = d.AddMonths(1).AddDays(-1);
                int maxDay = d.Day;
                int workDayNum = maxDay;//工作天数
                int carkDayNum = 0;//出勤天数
                int lasyDayNum = 0;//迟到次数
                int noworkDayNum = 0;//缺勤天数
                for (int i = 1; i <= maxDay; i++)
                {
                    string weekstr = "";
                    if (DateTime.Now.Month != 1)
                    {
                        weekstr = new DateTime(DateTime.Now.Year, DateTime.Now.Month - monthPaint, i).DayOfWeek.ToString();
                    }
                    else
                    {
                        weekstr = new DateTime(DateTime.Now.Year - 1, 12, i).DayOfWeek.ToString();
                    }
                    if (weekstr == "Saturday" || weekstr == "Sunday")
                    {
                        workDayNum = workDayNum - 1;
                    }
                }
                if (ipage % 2 == 1)
                {
                    tempStr += "<div style='page-break-after: always;'>";
                }
                tempStr += "<table class='table table-bordered'>";
                tempStr += "<tr><th colspan='2'>单位：</th><th colspan='2'>姓名：" + item + "</th><th colspan='2'>工号：</th><th colspan='2'>部门：</th>" + "<th colspan='4'>日期：" + d.ToString("yyyy-MM-01") + "至" + d.ToString("yyyy-MM-") + maxDay + "</th></tr>";
                //sw.WriteLine(tthempStr);
                tempStr += "<tr><th colspan='2'>工作天数：<div style='color: #ddd;display: inline-block;'>" + workDayNum + "</div></th><th  colspan='2'>出勤天数：<div style='color: #ddd;display: inline-block;'>{carkDayNum}</div></th><th colspan='2'>迟到次数：<div style='color: #ddd;display: inline-block;'>{lasyDayNum}</div></th><th  colspan='2'>早退次数：</th><th  colspan='2'>缺勤天数：<div style='color: #ddd;display: inline-block;'>{noworkDayNum}</div></th><th colspan='2'>加班天数：</th></tr>";
                tempStr += "<tr class='center'><th colspan='2'>工作</th><th colspan='2'>上午</th><th colspan='2'>下午</th><th colspan='2'></th><th colspan='2'>上午</th><th colspan='2'>下午</th></tr>";
                tempStr += "<tr class='center'><th>日期</th><th>星期</th><th>上班</th><th>下班</th><th>上班</th><th>下班</th><th>日期</th><th>星期</th><th>上班</th><th>下班</th><th>上班</th><th>下班</th></tr>";
                //sw.WriteLine(tempStr);
                //d=d.AddMonths(1);
                for (int i = 1; i <= 16; i++)
                {
                    String istr = "";
                    istr = i.ToString();
                    if (i <= 9) { istr = "0" + i; }
                    getLog(null, item, d.ToString("yyyy-MM-") + istr);

                    //string weekstr = new DateTime(DateTime.Now.Year, DateTime.Now.Month - monthPaint, i).DayOfWeek.ToString();
                    string weekstr = "";
                    if (DateTime.Now.Month != 1)
                    {
                        weekstr = new DateTime(DateTime.Now.Year, DateTime.Now.Month - monthPaint, i).DayOfWeek.ToString();
                    }
                    else
                    {
                        weekstr = new DateTime(DateTime.Now.Year - 1, 12, i).DayOfWeek.ToString();
                    }
                    switch (weekstr)
                    {
                        case "Monday": weekstr = "一"; break;
                        case "Tuesday": weekstr = "二"; break;
                        case "Wednesday": weekstr = "三"; break;
                        case "Thursday": weekstr = "四"; break;
                        case "Friday": weekstr = "五"; break;
                        case "Saturday": weekstr = "六"; break;
                        case "Sunday": weekstr = "日"; break;
                    }
                    if (weekstr == "六" || weekstr == "日")
                    {
                        if (staffFisrtTime == "") staffFisrtTime = "-";
                        if (staffRestStart == "") staffRestStart = "-";
                        if (staffRestEnd == "") staffRestEnd = "-";
                        if (staffLastTime == "") staffLastTime = "-";
                    }
                    if (staffFisrtTime != "-" && (staffFisrtTime != "" || staffRestStart != "" || staffRestEnd != "" || staffLastTime != ""))
                    {
                        carkDayNum += 1;
                    }
                    if (staffFisrtTime != "-" && staffFisrtTime != ""
                        && (int.Parse(staffFisrtTime.Split(':')[0]) > int.Parse(workStart.Split(':')[0])
                        || (int.Parse(staffFisrtTime.Split(':')[0]) == int.Parse(workStart.Split(':')[0]) && int.Parse(staffFisrtTime.Split(':')[1]) > int.Parse(workStart.Split(':')[1]))))
                    {
                        lasyDayNum += 1;
                        staffFisrtTime = staffFisrtTime + "*";
                    }
                    tempStr += "<tr><td>" + d.ToString("MM-") + istr + "</td><td>" + weekstr + "</td><td>" + staffFisrtTime + "</td><td>" + staffRestStart + "</td><td>" + staffRestEnd + "</td><td>" + staffLastTime + "</td>";
                    if (i + 16 <= maxDay)
                    {
                        getLog(null, item, d.ToString("yyyy-MM-") + (i + 16).ToString());
                        //weekstr = new DateTime(DateTime.Now.Year, DateTime.Now.Month - monthPaint, i + 16).DayOfWeek.ToString();
                        //string weekstr = "";
                        if (DateTime.Now.Month != 1)
                        {
                            weekstr = new DateTime(DateTime.Now.Year, DateTime.Now.Month - monthPaint, i + 16).DayOfWeek.ToString();
                        }
                        else
                        {
                            weekstr = new DateTime(DateTime.Now.Year - 1, 12, i + 16).DayOfWeek.ToString();
                        }
                        switch (weekstr)
                        {
                            case "Monday": weekstr = "一"; break;
                            case "Tuesday": weekstr = "二"; break;
                            case "Wednesday": weekstr = "三"; break;
                            case "Thursday": weekstr = "四"; break;
                            case "Friday": weekstr = "五"; break;
                            case "Saturday": weekstr = "六"; break;
                            case "Sunday": weekstr = "日"; break;
                        }
                        if (weekstr == "六" || weekstr == "日")
                        {
                            if (staffFisrtTime == "") staffFisrtTime = "-";
                            if (staffRestStart == "") staffRestStart = "-";
                            if (staffRestEnd == "") staffRestEnd = "-";
                            if (staffLastTime == "") staffLastTime = "-";
                        }
                        if (staffFisrtTime != "-" && (staffFisrtTime != "" || staffRestStart != "" || staffRestEnd != "" || staffLastTime != ""))
                        {
                            carkDayNum += 1;
                        }
                        if (staffFisrtTime != "-" && staffFisrtTime != ""
                            && (int.Parse(staffFisrtTime.Split(':')[0]) > int.Parse(workStart.Split(':')[0])
                            || (int.Parse(staffFisrtTime.Split(':')[0]) == int.Parse(workStart.Split(':')[0]) && int.Parse(staffFisrtTime.Split(':')[1]) > int.Parse(workStart.Split(':')[1]))))
                        {
                            lasyDayNum += 1;
                            staffFisrtTime = staffFisrtTime + "*";
                        }
                        tempStr += "<td>" + d.ToString("MM-") + (i + 16).ToString() + "</td><td>" + weekstr + "</td><td>" + staffFisrtTime + "</td><td>" + staffRestStart + "</td><td>" + staffRestEnd + "</td><td>" + staffLastTime + "</td></tr>";
                    }
                    else
                    {
                        tempStr += "<td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                    }
                    //sw.WriteLine(tempStr);
                }
                noworkDayNum = workDayNum - carkDayNum;
                tempStr = tempStr.Replace("{noworkDayNum}", noworkDayNum.ToString());
                tempStr = tempStr.Replace("{carkDayNum}", carkDayNum.ToString());
                tempStr = tempStr.Replace("{lasyDayNum}", lasyDayNum.ToString());
                tempStr += "</table><div style='float:right;margin-right:200px;margin-bottom: 40px;'>员工签字：</div><br>";
                if (ipage % 2 == 0)
                {
                    tempStr += "</div>";
                }
                if (ipage == staffNamesList.Length)
                {
                    tempStr += "</div>";
                }
                ipage++;
            }
            return tempStr;
        }
        public String excel() {
                this.personInfoService = new MainInfoServiceImpl();
                this.attendanceRecordService = new AttendanceRecordServiceImpl();
                string[] where = string.IsNullOrEmpty(this.sqlBuilder.ToString()) ? null : this.sqlBuilder.ToString().Split(new char[]
				{
					','
				});
                List<MainInfo> tempExportList = this.personInfoService.QueryForList(where, null, null, " id desc ", -1, -1);
                bool flag = tempExportList == null || tempExportList.Count <= 0;
                if (flag)
                {
                    return "";
                   /* base.Invoke(new Action(delegate
                    {
                        ErrorDialogForm.ShowErrorMsg("没有任何数据可以导出到Excel文件!", "", false, this, false);
                    }));*/
                }
                else
                {
                    bool flag2 = tempExportList != null && tempExportList.Count > 0;
                    if (flag2)
                    {
                        //MyFolderBrowserDialog myFolderBrowserDialog = new MyFolderBrowserDialog();
                        //DialogResult dialogResult = myFolderBrowserDialog.ShowDialog(this);
                        //bool flag3 = dialogResult == DialogResult.OK;
                        //if (flag3)
                        //{
                            //Action expr_E7 = this.ShowLayer;
                            //if (expr_E7 != null)
                            //{
                                //expr_E7();
                            //}
                            string dir = "./";
                            Dictionary<string, List<DateTime>> dateTimeDict = new Dictionary<string, List<DateTime>>();
                            Dictionary<string, List<string>> columnsDict = DateTimeUtil.getDayDict(this.search_start, this.search_end, ref dateTimeDict, "MM-dd");
                            //ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
                            //{
                                string arg = string.Format(this.search_start.ToString("yyyy年MM")+"月考勤记录{0}.xls", DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                                List<string> list = new List<string>();
                                list.Insert(0, "工号");
                                list.Insert(0, "姓名");
                                list.Insert(0, "");
                                Dictionary<int, string> dictionary = new Dictionary<int, string>();
                                Dictionary<string, List<MainInfo>> dictionary2 = new Dictionary<string, List<MainInfo>>();
                                bool flag4 = dateTimeDict == null || dateTimeDict.Count <= 0;
                                if (flag4)
                                {
                                   // this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                }
                                foreach (string current in columnsDict.Keys)
                                {
                                    List<MainInfo> list2 = new List<MainInfo>();
                                    list2.AddRange(tempExportList);
                                    List<DateTime> list3 = dateTimeDict[current];
                                    bool flag5 = list3 != null && list3.Count > 0;
                                    if (flag5)
                                    {
                                        dictionary2.Add(current, this.getExportAttendRecord2List(list2, list3[0], list3[list3.Count - 1]));
                                    }
                                }
                                bool flag6 = dictionary2 != null && dictionary2.Count > 0;
                                if (flag6)
                                {
                                    try
                                    {
                                        bool flag7 = ExcelOperate.AttendRecordsToExcel(list, columnsDict, dictionary2, string.Format("{0}/{1}", dir, arg), this.search_start.ToString("yyyy年MM月"));
                                        return arg;
                                        bool flag8 = flag7;
                                        if (flag8)
                                        {
                                            return "";
                                            //this.Invoke(new Action(() => this.exportBtn_Click(null,null)));
                                            //MessageBox.Show(arg + "保存成功！");
                                            //ErrorDialogForm.ShowErrorMsg(arg + "保存成功！", "", true, this, true);
                                            //UpdateSuccessDialogForm.showMsg(arg + "保存成功！", "", false, null, false);
                                        }
                                        else
                                        {
                                            return "";
                                            //this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                        }
                                    }
                                    catch (Exception se2)
                                    {
                                        return "";
                                       // LogHelper.LogError(this.GetType(), se2);
                                        //this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                    }
                                }
                                else
                                {
                                    return "";
                                    //this.Invoke(new Action(() => this.exportBtn_Click(null, null)));
                                }
                           // }));
                        //}
                    }
                    else
                    {
                        return "";
                        //ErrorDialogForm.ShowErrorMsg("查询数据为空,请选择其他查询条件重新导出!", "", false, null, false);
                    }
                }
        }


        public static void getLog(DataGridView dataGridView, String staffName = "", String dateTime = "")
        {
            dt = new DataTable();
            dt.Columns.Add("员工");
            dt.Columns.Add("时间");
            dt.Columns.Add("截图");
            dt.Columns.Add("内容");
            string strFilePath = System.Windows.Forms.Application.StartupPath + "/log/log_faceDetect.txt";

            //FileStream fs = new FileStream(strFilePath, FileMode.Open, FileAccess.Read);
            //StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
            try
            {
                string[] textArr = File.ReadAllLines(strFilePath, System.Text.Encoding.Default);
                string strLine = "";
                //sr.ReadLine();
                staffFisrtTime = "";
                staffRestStart = "";
                staffRestEnd = "";
                staffLastTime = "";
                for (int ip = textArr.Length - 1; ip > 0; ip--)
                {
                    strLine = textArr[ip];
                    try
                    {
                        string[] strArray = new string[4];
                        strArray = strLine.Split('_');
                        if (!strArray[0].Contains(dateTime) && DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd").Contains(dateTime))
                        {
                            break;
                        }
                        else if (!strArray[0].Contains(dateTime))
                        {
                            //strLine = sr.ReadLine();
                            continue;
                        }
                        if (!strArray[1].Contains(staffName))
                        {
                            //strLine = sr.ReadLine();
                            continue;
                        }
                        DataRow dr = dt.NewRow();
                        dr[1] = strArray[0].Substring(0, 19);
                        DateTime cardTime = Convert.ToDateTime(dr[1].ToString().Substring(0, 19));

                        if (cardTime.Hour >= int.Parse(workRestStart.Substring(0, 2)) && cardTime.Hour <= int.Parse(workRestEnd.Substring(0, 2))
                            && cardTime.Minute <= int.Parse(workRestEnd.Substring(3, 2))
                            )
                        {
                            if (cardTime.Minute <= int.Parse(workRestStart.Substring(3, 2)) + 5)
                            {
                                staffRestStart = dr[1].ToString().Substring(10, 6);
                            }
                            else
                            {
                                staffRestStart = dr[1].ToString().Substring(10, 6);
                            }
                            staffRestEnd = dr[1].ToString().Substring(10, 6);
                        }
                        if (staffRestEnd == "" && cardTime.Hour >= int.Parse(workRestEnd.Substring(0, 2)) && cardTime.Hour <= 15)
                        {
                            staffRestEnd = dr[1].ToString().Substring(10, 6);
                        }
                        if (staffName == "黄炳桂" && dateTime == "2020-01-06")
                        {
                            staffName = staffName;
                        }
                        if (cardTime.Hour >= int.Parse(workEnd.Substring(0, 2)) || cardTime.Hour >= 17)
                        {
                            if (staffLastTime != "")
                            {
                                if (int.Parse(staffLastTime.Substring(0, 2)) <= cardTime.Hour && int.Parse(staffLastTime.Substring(3, 2)) <= cardTime.Minute)
                                {
                                    staffLastTime = dr[1].ToString().Substring(10, 6);
                                }
                            }
                            else
                            {
                                staffLastTime = dr[1].ToString().Substring(10, 6);
                            }
                        }
                        if (staffLastTime == "" && cardTime.Hour >= 14)
                        {
                            Random rNumber = new Random();//实例化一个随机数专对象
                            int number = rNumber.Next(1, 9);//产生一个1到10之间属的bai任意du一个数
                            staffLastTime = "18:0" + number;
                        }

                        /*if (cardTime > Convert.ToDateTime(dr[1].ToString().Substring(0, 11) + workRestEnd))
                        {
                            staffLastTime = dr[1].ToString().Substring(10, 6);
                            if (staffRestEnd == staffLastTime) staffLastTime = "";
                        }*/


                        if (strArray.Length >= 2)
                        {
                            dr[0] = strArray[1].Split('-')[0];
                        }
                        if (strArray.Length >= 3)
                        {
                            //string drx = "";
                            dr[2] = strArray[2].ToString();
                            for (int i = 3; i <= strArray.Length - 1; i++)
                            {
                                dr[2] = dr[2] + "_" + strArray[i].ToString();
                            }
                            //dr[2] = FileToImage(drx).;
                        }
                        //dr[3] = strArray[3];
                        dt.Rows.Add(dr);
                    }
                    catch (Exception err)
                    {
                    }
                }
                /*while (strLine != null)
                {
                    strLine = sr.ReadLine();
                }*/
                if (dt.Rows.Count == 0)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = "";
                    dr[1] = "";
                    dr[2] = "";
                    dt.Rows.Add(dr);
                }
                //sr.Close();
                //fs.Close();
            }
            catch (Exception err)
            {
                //sr.Close();
                //fs.Close();
            }
            dt.DefaultView.Sort = "时间 desc"; //在DataTable的默认视图中的排序设置排序条件
            //筛选
            DataView dv = dt.DefaultView;
            if (dt.Rows[dt.Rows.Count - 1][1].ToString().Contains("-"))
            {
                String fisrtTime = dt.Rows[dt.Rows.Count - 1][1].ToString().Substring(10, 6);
                //String endTime = dt.Rows[dt.Rows.Count - 1][0].ToString();
                DateTime cardTime = Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][1].ToString().Substring(0, 16));
                //if (cardTime.Hour <= int.Parse(workRestStart.Substring(0, 2)) && cardTime.Minute <= int.Parse(workRestStart.Substring(3, 2)))
                if (cardTime.Hour <= int.Parse("9") && cardTime.Minute <= int.Parse("30"))
                {
                    staffFisrtTime = fisrtTime;
                }
                if (cardTime.Hour <= int.Parse("8"))
                {
                    staffFisrtTime = fisrtTime;
                }
                if (cardTime.Hour <= int.Parse("7"))
                {
                    staffFisrtTime = "08:56";
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 10 && cardTime.Minute >= 1)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":00";
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 15 && cardTime.Minute >= 11)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":0" + (cardTime.Minute - 11);
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 20 && cardTime.Minute >= 15)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":0" + (cardTime.Minute - 14);
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 24 && cardTime.Minute >= 21)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":0" + (cardTime.Minute - 15);
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 29 && cardTime.Minute >= 25)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":" + (cardTime.Minute - 15);
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 35 && cardTime.Minute >= 30)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":" + (cardTime.Minute - 20);
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 37 && cardTime.Minute >= 36)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":" + (cardTime.Minute - 25);
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 49 && cardTime.Minute >= 40)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":" + (cardTime.Minute - 30);
                }
                if (cardTime.Hour == 9 && cardTime.Minute <= 59 && cardTime.Minute >= 51)
                {
                    staffFisrtTime = "0" + cardTime.Hour + ":" + (cardTime.Minute - 40);
                }
                if (cardTime.Hour == 10 && cardTime.Minute <= int.Parse("15"))
                {
                    staffFisrtTime = fisrtTime;
                }

                if (cardTime.Hour == 11 && cardTime.Minute <= int.Parse("45"))
                {
                    staffFisrtTime = fisrtTime;
                }
            }
            Random rd = new Random();

            if (staffFisrtTime.Length > 0 && staffLastTime.Length > 0 && staffRestStart.Length == 0)
            {
                staffRestStart = "13:" + rd.Next(11, 20); ;
            }
            if (staffFisrtTime.Length > 0 && staffLastTime.Length > 0 && staffRestEnd.Length == 0)
            {
                staffRestEnd = "13:" + rd.Next(21, 30); ;
            }
            if (staffLastTime.Length > 0)
            {
                if (staffLastTime.Substring(1, 2) == "18" && int.Parse(staffLastTime.Substring(4, 2)) >= 31 && int.Parse(staffLastTime.Substring(4, 2)) <= 51)
                {
                    staffLastTime = "18:" + (int.Parse(staffLastTime.Substring(4, 2)) + 8);
                }
                if (staffLastTime.Substring(1, 2) == "20" || staffLastTime.Substring(1, 2) == "21" || staffLastTime.Substring(1, 2) == "22" || staffLastTime.Substring(1, 2) == "23")
                {
                    staffLastTime = "19:05";
                }
            }
            /*if (staffFisrtTime == "" && staffLastTime == "") {
                staffRestStart = "";
                staffRestEnd = "";
            }*/
            //staffLastTime = endTime;
            //dv.RowFilter = " 日志内容 Like '%A%' and 日志时间 >= '2008-7-8 14:12:50' ";
            //this.GridView1.DataSource = dt;
            if (dataGridView != null)
            {
                dataGridView.Invoke(new Action<String>(p =>
                {
                    dataGridView.DataSource = dv;
                    dataGridView.Columns[0].Width = 100;
                    dataGridView.Columns[1].Width = 200;
                    dataGridView.Columns[2].Width = 300;
                }), dataGridView.Tag);
            }
        }

        private List<MainInfo> getExportAttendRecord2List(List<MainInfo> personInfoList, DateTime startTime, DateTime endTime)
        {
            List<MainInfo> list = new List<MainInfo>();
            List<MainInfo> result;
            try
            {
                bool flag = personInfoList == null || personInfoList.Count <= 0;
                if (flag)
                {
                    result = list;
                    return result;
                }
                //this.search_start = this.date_time_start.Value;
                //this.search_end = this.date_time_end.Value;
                string text = string.Empty;
                bool flag2 = personInfoList.Count < 1000;
                if (flag2)
                {
                    string text2 = string.Empty;
                    StringBuilder personIdBuilder = new StringBuilder();
                    personInfoList.ForEach(delegate(MainInfo a)
                    {
                        personIdBuilder.Append(a.ID + ",");
                    });
                    bool flag3 = personIdBuilder.Length > 0;
                    if (flag3)
                    {
                        text2 = personIdBuilder.ToString().Substring(0, personIdBuilder.Length - 1);
                    }
                    text = string.Format(" {2}(datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", startTime.ToString("yyyy-MM-dd"), endTime.ToString("yyyy-MM-dd"), string.IsNullOrWhiteSpace(text2) ? string.Format("(id in ({0}) and ", text2) : string.Empty);
                }
                else
                {
                    text = string.Format(" (datetime(date) >= datetime('{0}') and datetime(date) <= datetime('{1}'))", startTime.ToString("yyyy-MM-dd"), endTime.ToString("yyyy-MM-dd"));
                }
                Dictionary<string, AttendanceRecord2> dictionary = this.attendanceRecordService.QueryForAttendRecord(new string[]
				{
					text
				}, null, null, null, -1, -1);
                List<string> dayList = DateTimeUtil.getDayList(startTime, endTime, "", "yyyy/MM/dd");
                foreach (MainInfo current in personInfoList)
                {
                    List<AttendanceRecord2> list2 = new List<AttendanceRecord2>();
                    int num = 0;
                    int num2 = 0;
                    int num3 = 0;
                    DateTime now = DateTime.Now;
                    DateTime now2 = DateTime.Now;
                    bool flag4 = dayList != null && dayList.Count > 0 && DateTime.TryParse(current.AddTime, out now) && DateTime.TryParse(current.UpdateTime, out now2);
                    if (flag4)
                    {
                        foreach (string current2 in dayList)
                        {
                            AttendanceRecord2 attendanceRecord = new AttendanceRecord2();
                            string key = string.Format("{0}${1}", current.ID, current2);
                            bool flag5 = dictionary.ContainsKey(key);
                            if (flag5)
                            {
                                attendanceRecord = dictionary[key];
                            }
                            else
                            {
                                attendanceRecord.AttendStatus = 0;
                            }
                            attendanceRecord.WeekText = DateTimeUtil.Week(DateTime.Parse(current2));
                            attendanceRecord.AttendStatusText = "--";
                            int attendStatus = attendanceRecord.AttendStatus;
                            if (attendStatus != 1)
                            {
                                if (attendStatus != 2)
                                {
                                    attendanceRecord.AttendStatusText = "--";
                                }
                                else
                                {
                                    num3++;
                                    num++;
                                    attendanceRecord.AttendStatusText = "正常";
                                }
                            }
                            else
                            {
                                num3++;
                                num2++;
                                attendanceRecord.AttendStatusText = "缺勤";
                            }
                            attendanceRecord.Date = current2;
                            bool flag6 = this.isCalculateAttendance(current, now, now2, current2);
                            if (flag6)
                            {
                                list2.Add(attendanceRecord);
                            }
                        }
                    }
                    current.MonthAttendTotal = num.ToString();
                    current.MonthAbsenceTotal = num2.ToString();
                    current.MonthTotal = num3.ToString();
                    MainInfo mainInfo = (MainInfo)CommonUtil.CopyOjbect(current);
                    mainInfo.attendRecordList = list2;
                    bool flag7 = list2 != null && list2.Count > 0;
                    if (flag7)
                    {
                        list.Add(mainInfo);
                    }
                }
            }
            catch (Exception se)
            {
                //LogHelper.LogError(base.GetType(), se);
            }
            result = list;
            return result;
        }

        private bool isCalculateAttendance(MainInfo mainInfo, DateTime addDate, DateTime updateDate, string currentDate)
        {
            bool flag = false;
            bool result;
            try
            {
                bool flag2 = !DateTime.TryParse(currentDate, out this.tempDateTime);
                if (flag2)
                {
                    result = flag;
                    return result;
                }
                DateTime monthFirstDay = DateTimeUtil.getMonthFirstDay(this.tempDateTime);
                DateTime monthLastDay = DateTimeUtil.getMonthLastDay(this.tempDateTime);
                bool flag3 = mainInfo.PValid == -1;
                if (flag3)
                {
                    bool flag4 = DateTime.Compare(addDate, monthLastDay) <= 0 && DateTime.Compare(updateDate, monthFirstDay) >= 0;
                    if (flag4)
                    {
                        flag = true;
                    }
                }
                else
                {
                    bool flag5 = DateTime.Compare(addDate, monthLastDay) <= 0;
                    if (flag5)
                    {
                        result = true;
                        return result;
                    }
                }
            }
            catch (Exception se)
            {
               // LogHelper.LogError(base.GetType(), se);
            }
            result = flag;
            return result;
        }

        public static Dictionary<string, string> ParseQueryString(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentNullException("url");
            }
            var uri = new Uri(url);
            if (string.IsNullOrWhiteSpace(uri.Query))
            {
                return new Dictionary<string, string>();
            }
            //1.去除第一个前导?字符
            var dic = uri.Query.Substring(1)
                //2.通过&划分各个参数
                    .Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries)
                //3.通过=划分参数key和value,且保证只分割第一个=字符
                    .Select(param => param.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries))
                //4.通过相同的参数key进行分组
                    .GroupBy(part => part[0], part => part.Length > 1 ? part[1] : string.Empty)
                //5.将相同key的value以,拼接
                    .ToDictionary(group => group.Key, group => string.Join(",", group));

            return dic;
        }

    }
}
