
using LogUtil;
using System;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Common
{
	internal class MyDragOpaqueLayer
	{
		private delegate void InvokeDelegate();

		private Form form = null;

		private PictureBox pic = new PictureBox();

		private bool isRefersh = true;

		private System.Timers.Timer timer = new System.Timers.Timer();

		private object refershLocker = new object();

		public void ShowOpaqueLayer(Form control, double alpha, Color backColor)
		{
			try
			{
				bool flag = this.form == null;
				if (flag)
				{
					this.form = new Form();
					this.pic.Width = 39;
					this.pic.Height = 39;
					bool flag2 = this.pic != null;
					if (flag2)
					{
						this.pic.Width = 39;
						this.pic.Height = 39;
						this.pic.SizeMode = PictureBoxSizeMode.Zoom;
						//this.pic.BackgroundImage = Resources.load_0;
					}
					this.form.Controls.Add(this.pic);
					this.form.Opacity = alpha;
					this.form.BackColor = backColor;
					this.form.FormBorderStyle = FormBorderStyle.None;
					this.form.ShowInTaskbar = false;
					control.AddOwnedForm(this.form);
					this.form.BringToFront();
					this.timer = new System.Timers.Timer();
					this.timer.Elapsed += new ElapsedEventHandler(this.Timer_Elapsed);
					this.timer.Interval = 250.0;
					this.timer.Enabled = true;
					this.form.TopMost = true;
					this.form.Show();
				}
				this.form.Width = control.Width;
				this.form.Height = control.Height;
				this.form.Location = control.Location;
				this.pic.Location = new Point(this.form.Width / 2 - 19, this.form.Height / 2 - 19);
				this.form.Enabled = true;
				this.form.Visible = true;
				this.timer.Enabled = true;
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			try
			{
				this.isRefersh = !this.isRefersh;
				bool flag = this.pic != null;
				if (flag)
				{
					//this.pic.BackgroundImage = (this.isRefersh ? Resources.load_0 : Resources.load_180);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void InvokeMethod()
		{
			this.isRefersh = !this.isRefersh;
			bool flag = this.pic != null;
			if (flag)
			{
				//this.pic.BackgroundImage = (this.isRefersh ? Resources.load_0 : Resources.load_180);
			}
		}

		public void HideOpaqueLayer()
		{
			try
			{
				bool flag = this.form != null;
				if (flag)
				{
					this.form.Visible = false;
					this.form.Enabled = false;
					this.timer.Enabled = false;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}
	}
}
