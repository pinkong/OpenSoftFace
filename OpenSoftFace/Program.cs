﻿using OpenSoftFace.RestService;
using OpenSoftFace.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenSoftFace
{
    static class Program
    {
        public static FaceForm faceForm=null;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            /*Task.Run(() =>
            {
                StartService();
            });*/
            Task.Run(() =>
            {
                WebHttpService.run();
            });
            faceForm = new FaceForm();
            Application.Run(faceForm);
            
        }

        public static void StartService(){
             try
            {
                InfoQueryServices service = new InfoQueryServices();
                WebServiceHost _serviceHost = new WebServiceHost(service, new Uri("http://127.0.0.1/"));
                _serviceHost.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Web服务开启失败：{0}\r\n{1}", ex.Message, ex.StackTrace);
            }

        }
    }
}
