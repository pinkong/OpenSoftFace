using OpenSoftFace.Setting.Controls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Forms
{
	public class ConfirmDialogForm : DragBaseForm
	{
		public delegate DialogResult InvokeDelegate(Form parent, Form subForm);

		private static ConfirmDialogForm confirmForm;

		private static DialogResult dialog;

		public static int labelWidth = 10;

		private IContainer components = null;

		private MyButton btn_ok;

		private MyButton btn_cancel;

		private Label label1;

		private Label lbl_errorMsg;

		private PictureBox pic_error;

		public ConfirmDialogForm()
		{
			this.InitializeComponent();
		}

		public static DialogResult showWarningMsg()
		{
			ConfirmDialogForm.confirmForm = new ConfirmDialogForm();
			ConfirmDialogForm.confirmForm.TopMost = true;
			ConfirmDialogForm.confirmForm.ShowDialog();
			return ConfirmDialogForm.dialog;
		}

		private void ok_Click(object sender, EventArgs e)
		{
			ConfirmDialogForm.dialog = DialogResult.OK;
			ConfirmDialogForm.confirmForm.DialogResult = ConfirmDialogForm.dialog;
			ConfirmDialogForm.confirmForm.Close();
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			ConfirmDialogForm.dialog = DialogResult.Cancel;
			ConfirmDialogForm.confirmForm.DialogResult = ConfirmDialogForm.dialog;
			ConfirmDialogForm.confirmForm.Close();
		}

		public override void Close_btn_Click(object sender, EventArgs e)
		{
			ConfirmDialogForm.dialog = DialogResult.None;
			ConfirmDialogForm.confirmForm.DialogResult = ConfirmDialogForm.dialog;
			ConfirmDialogForm.confirmForm.Close();
		}

		public static DialogResult XShowDialog(Form parent, Form subForm)
		{
			bool invokeRequired = parent.InvokeRequired;
			DialogResult result;
			if (invokeRequired)
			{
				ConfirmDialogForm.InvokeDelegate method = new ConfirmDialogForm.InvokeDelegate(ConfirmDialogForm.XShowDialog);
				parent.Invoke(method, new object[]
				{
					parent,
					subForm
				});
				result = subForm.DialogResult;
			}
			else
			{
				DialogResult dialogResult = subForm.ShowDialog(parent);
				result = dialogResult;
			}
			return result;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmDialogForm));
            this.btn_ok = new OpenSoftFace.Setting.Controls.MyButton();
            this.btn_cancel = new OpenSoftFace.Setting.Controls.MyButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_errorMsg = new System.Windows.Forms.Label();
            this.pic_error = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_error)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.label1);
            this.splitContainer.Panel2.Controls.Add(this.lbl_errorMsg);
            this.splitContainer.Panel2.Controls.Add(this.pic_error);
            this.splitContainer.Panel2.Controls.Add(this.btn_ok);
            this.splitContainer.Panel2.Controls.Add(this.btn_cancel);
            this.splitContainer.Size = new System.Drawing.Size(375, 248);
            this.splitContainer.SplitterDistance = 52;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(350, 9);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            // 
            // lbl_title
            // 
            this.lbl_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.lbl_title.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl_title.Size = new System.Drawing.Size(375, 52);
            this.lbl_title.Text = "系统提示";
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_ok.EnterForeColor = System.Drawing.Color.White;
            this.btn_ok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_ok.ForeColor = System.Drawing.Color.White;
            this.btn_ok.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.LeaveForeColor = System.Drawing.Color.White;
            this.btn_ok.Location = new System.Drawing.Point(198, 136);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(92, 32);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "立即重启";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.White;
            this.btn_cancel.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_cancel.EnterForeColor = System.Drawing.Color.Black;
            this.btn_cancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.btn_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cancel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_cancel.ForeColor = System.Drawing.Color.Black;
            this.btn_cancel.LeaveBackColor = System.Drawing.Color.White;
            this.btn_cancel.LeaveForeColor = System.Drawing.Color.Black;
            this.btn_cancel.Location = new System.Drawing.Point(85, 136);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(92, 32);
            this.btn_cancel.TabIndex = 1;
            this.btn_cancel.Text = "暂不重启";
            this.btn_cancel.UseVisualStyleBackColor = false;
            this.btn_cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(105, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "设置内容将在程序重启后生效";
            // 
            // lbl_errorMsg
            // 
            this.lbl_errorMsg.AutoEllipsis = true;
            this.lbl_errorMsg.AutoSize = true;
            this.lbl_errorMsg.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lbl_errorMsg.Location = new System.Drawing.Point(146, 15);
            this.lbl_errorMsg.Name = "lbl_errorMsg";
            this.lbl_errorMsg.Size = new System.Drawing.Size(82, 24);
            this.lbl_errorMsg.TabIndex = 9;
            this.lbl_errorMsg.Text = "设置完成";
            // 
            // pic_error
            // 
            this.pic_error.Location = new System.Drawing.Point(167, 47);
            this.pic_error.Name = "pic_error";
            this.pic_error.Size = new System.Drawing.Size(40, 40);
            this.pic_error.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_error.TabIndex = 10;
            this.pic_error.TabStop = false;
            // 
            // ConfirmDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(375, 248);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ConfirmDialogForm";
            this.ShowInTaskbar = false;
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_error)).EndInit();
            this.ResumeLayout(false);

		}
	}
}
